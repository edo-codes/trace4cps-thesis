////
  // Copyright (c) 2021 Contributors to the Eclipse Foundation
  //
  // This program and the accompanying materials are made
  // available under the terms of the Eclipse Public License 2.0
  // which is available at https://www.eclipse.org/legal/epl-2.0/
  //
  // SPDX-License-Identifier: EPL-2.0
////

include::_attributes.asciidoc[]

= Developing

include::dev-env-setup.asciidoc[]

[[maven-build]]
== Building with Maven

[CAUTION]
====
{trace} should be built using a _Java 1.8 VM_.
The JDK can be downloaded from e.g. https://www.oracle.com/technetwork/pt/java/javase/downloads/jdk8-downloads-2133151.html[Oracle] or https://adoptium.net/archive.html?variant=openjdk8&jvmVariant=hotspot[Adoptium]. +
To test which Java version is used by Maven, type `mvn -version` in a command shell.
====

To build {trace} with Maven execute the following command in the root:

Linux :: `./build.sh`
Windows :: `.\build.cmd`
Other :: `mvn -Dtycho.pomless.aggregator.names=releng,temporallogic,trace,jfreechart`

NOTE: In the remainder of this document this command will be referred to as `build.sh`

== License header

The <<maven-build, Maven build>> uses https://github.com/mycila/license-maven-plugin[license-maven-plugin] to determine if the link:../releng/legal-defaults/license-header.txt[correct license headers] are used for source files.
If the header is incorrect the build fails.

Handy commands:

- To only run the check execute: `./build.sh license:check`.
- To automatically add/update execute: `./build.sh license:format`.

== Third party notice

Whenever dependencies change, link:../NOTICE.asciidoc[NOTICE.asciidoc] has to be updated.

== Design

=== Methodology

The figure below shows a mind map on the ingredients of the TRACE tool, according to the separation in four methodological elements: formalisms, techniques, methods and tools.

image::methodology.png[]

[[code-overview]]
=== Code overview

The TRACE tool is set up as an Eclipse feature consisting of several Eclipse plugins.
Some of these plugins are plain jar files (OSGi bundles) and have no Eclipse-specific dependencies.
The following is an overview:

* `docs`: contains documentation for developers
* `jfreechart/org.eclipse.trace4cps.common.jfreechart`: Some extensions of JFreeChart.
* `jfreechart/org.eclipse.trace4cps.common.jfreechart.feature`: A feature for the JFreeChart-specific bundles.
* `jfreechart/org.eclipse.trace4cps.common.jfreechart.ui`: Eclipse integration of JFreeChart.
* `jfreechart/org.jfree.jfreechart`: OSGi wrapper around the JFreeChart visualization library
* `releng/legal-defaults`: contains files with defaults for legal stuff.
* `releng/org.eclipse.trace4cps.configuration`: The master pom.xml file, containing the build configuration.
* `releng/org.eclipse.trace4cps.repository`: Standard update site for {trace}
* `releng/org.eclipse.trace4cps.target`: Standard Eclipse target platform configurations for {trace}
* `temporallogic/org.eclipse.trace4cps.tl`: Grammar definition of temporal-logic DSL
* `temporallogic/org.eclipse.trace4cps.tl.cmd`: Module for a command-line trace verification tool
* `temporallogic/org.eclipse.trace4cps.tl.ide`: Default Xtext IDE module
* `temporallogic/org.eclipse.trace4cps.tl.tests`: Unit tests for the temporal-logic DSL
* `temporallogic/org.eclipse.trace4cps.tl.ui`: Default Xtext UI module
* `trace/org.eclipse.trace4cps.core`: Core TRACE library with (i) data model, (ii) analysis algorithms, and (iii) basic persistence functionality.
* `trace/org.eclipse.trace4cps.core.tests`: Unit tests for the core.
* `trace/org.eclipse.trace4cps.feature`: The description of the TRACE feature.
* `trace/org.eclipse.trace4cps.ui`: Eclipse-based UI.
* `trace/org.eclipse.trace4cps.ui.tests`: Some unit tests intended to run from the IDE to manually inspect visualizations.
* `trace/org.eclipse.trace4cps.vis`: Core functionality for visualization. This library only depends on the JFreeChart library and can be used without Eclipse for visualizing traces.
* `trace/org.eclipse.trace4cps.vis.tests`: Some unit tests for the vis library.
* `trace-examples`: Example input files for the TRACE tool
* `pom.xml`: The root pom.xml file.

The following figure shows the dependencies between the most important components:

image::trace_components.png[]

The following guidelines have been followed:

Minimize proprietary lock-in :: 
The core of the trace tool (data model and analysis algorithms) must not be tied to specific frameworks, e.g., the Eclipse framework.
This allows deployment of the TRACE core functionality in a wide range of environments.
An example of this is the integration of the TRACE core in a JetBrains MPS environment for one of ESI’s customers.
The trace4cps.core library and the trace4cps.vis library only depend on a Java Runtime Environment and are thus widely usable, such as in MPS.

Provide a clear API ::
The TRACE API should be clear and managed.
Currently everything in the trace4cps.core component can be regarded as API.
The API for the data model and the persistence is seen to be rather clean and stable.
The API for the analysis algorithms could be improved.
 
The figure below shows the most important types of the TRACE data model. Note that Psop stands for “piecewise second-order polynomial” and this is the type that encodes the continuous real-valued signals.

image::trace_core_classes.png[]

The classes that implement these interfaces are also present in the trace4cps.core component, and these can be used to programmatically create a trace.
Note that there also are several extensions of ITrace that adds mutability (ITrace is a read-only trace).

=== Comments on implementation details

Some core classes are the following:

* trace4cps.core:
** TraceReader and TraceWriter provide persistence
** ModifiableTrace can be wrapped around an ITrace to make it mutable, add filters, etc.
** StlBuilder and MtlBuilder are used for the programmatic construction of temporal-logic formulas.
   The temporal logic DSL uses these builders to construct Formula instances from an Xtext specification.

* trace4cps.vis:
** TracePlotManager is the entry point for visualization of traces
** TraceViewConfiguration is the class that stores how a trace is shown (filters, coloring, grouping, highlights, etc.)
** DefaultDataItemFactory and DefaultToolTipGenerator are used by the TracePlotManager.
   If the TracePlotManager is used in an Eclipse context (see the TraceViewer class in trace4cps.ui), then Eclipse-specific classes are used: the EclipseDataItemFactory and the EclipseToolTipGenerator.
   These Eclipse-specific classes provide more functionality, e.g., integration with the Eclipse Properties view.
** ClaimFragment is used for explicit visualization of overlapping claims in the resource view or in the activity view with claim scaling set to FULL.
   Overlapping claims are split into fragments with an offset and a fraction such that the fragments of the claims do not overlap in the visualization.
   The ClaimCell class creates the fragments.
   The TracePlotManager distinguishes between regular IClaim instances, and ClaimFragment instances when generating the JFreeChart data items.

* trace4cps.ui: +
+
NOTE: The interplay between the TracePlotManager, TraceViewer and TraceView is rather complicated.
      This may be improved.

** TraceViewer is a JFace UI element that provides some JFreeChart – Eclipse integration. It encapsulates a TracePlotManager.
** TraceView is the implementation of the Eclipse IEditorPart that provides the editor for traces in the Eclipse UI.
   It encapsulates a TraceViewer.
*** The fillToolBar method creates the toolbar in the editor part.
    From here you can trace how the buttons are implemented (usage of the TRACE analysis API).
