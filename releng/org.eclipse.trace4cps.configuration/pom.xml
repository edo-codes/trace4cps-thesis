<!--

    Copyright (c) 2021 Contributors to the Eclipse Foundation

    This program and the accompanying materials are made
    available under the terms of the Eclipse Public License 2.0
    which is available at https://www.eclipse.org/legal/epl-2.0/

    SPDX-License-Identifier: EPL-2.0

-->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>org.eclipse.trace4cps</groupId>
    <artifactId>org.eclipse.trace4cps.configuration</artifactId>
    <version>0.2.0-SNAPSHOT</version>
    <packaging>pom</packaging>

    <properties>
        <!-- The end-user readable version number. -->
        <!-- The default is 'dev'. Jenkinsfile overrides this for releases only, to e.g. v0.1, v0.1-M1, v0.1-RC1. -->
        <trace4cps.version.enduser>dev</trace4cps.version.enduser>

        <!-- The version qualifier to use for all plugins/features/etc. -->
        <!-- Remains 'dev' for non-Jenkins builds. Jenkinsfile overrides the qualifier to e.g., -->
        <!-- 'v20210609-141908-dev' for development builds, 'v20210609-141908-M1' for milestone releases, -->
        <!-- 'v20210609-141908-RC1' for release candidates, and 'v20210609-141908' for final releases. -->
        <trace4cps.version.qualifier>dev</trace4cps.version.qualifier>

        <!-- The prefix to use for released binaries -->
        <trace4cps.binary.prefix>eclipse-trace4cps-incubation</trace4cps.binary.prefix>

        <!-- Building Java8 compliant code -->
        <java.source>1.8</java.source>
        <maven.compiler.source>${java.source}</maven.compiler.source>
        <maven.compiler.target>${java.source}</maven.compiler.target>

        <!-- Ensure platform independent build by fixing encoding. Prevents warnings. -->
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.build.resourceEncoding>UTF-8</project.build.resourceEncoding>

        <!-- Tycho version to use. -->
        <tycho.version>1.7.0</tycho.version>
        <tycho.extras.version>${tycho.version}</tycho.extras.version>
        <!-- Allow feature modules to override default behavior -->
        <source.feature.skip>false</source.feature.skip>

        <!-- Xtext/Xtend versions for Eclipse 2020-06 -->
        <xtext.version>2.22.0</xtext.version>
        <mwe2.version>2.11.3</mwe2.version>

        <!-- Eclipse Dash license tool. -->
        <eclipse.dash.license.tool.version>0.0.1-SNAPSHOT</eclipse.dash.license.tool.version>

        <!-- Documentation plug-in versions. -->
        <asciidoctor.maven.plugin.version>2.1.0</asciidoctor.maven.plugin.version>
        <asciidoctorj.version>2.4.3</asciidoctorj.version>
        <asciidoctorj.diagram.version>2.1.0</asciidoctorj.diagram.version>
        <asciidoctorj.pdf.version>1.5.4</asciidoctorj.pdf.version>
        <jruby.version>9.2.15.0</jruby.version>
        <geneclipsetoc.maven.plugin.version>1.0.3</geneclipsetoc.maven.plugin.version>

        <!-- Checkstyle version. Should match version used by Eclipse Checkstyle Plugin. -->
        <maven.checkstyle.version>3.1.2</maven.checkstyle.version>
        <checkstyle.version>8.41</checkstyle.version>

        <!-- Eclipse Common Build Infrastructure version. -->
        <eclipse.cbi.version>1.3.1</eclipse.cbi.version>

        <!-- https://mycila.carbou.me/license-maven-plugin/ -->
        <license.maven.plugin.version>4.2.rc2</license.maven.plugin.version>
    </properties>

    <pluginRepositories>
        <!-- Dash license plugin. -->
        <!-- Given that this contains snapshots, this must be first. Looking for snapshots on release repos will give an
            error, regardless of whether the plugin is present there or not. -->
        <pluginRepository>
            <id>dash-licenses-snapshots</id>
            <url>https://repo.eclipse.org/content/repositories/dash-licenses-snapshots/</url>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
        </pluginRepository>

        <!-- Eclipse CBI releases. For CBI signing plugins. -->
        <pluginRepository>
            <id>eclipse.cbi</id>
            <url>https://repo.eclipse.org/content/repositories/cbi-releases/</url>
        </pluginRepository>
    </pluginRepositories>


    <build>
        <defaultGoal>clean verify</defaultGoal>

        <plugins>
            <!-- Enforce minimum Maven 3.5 -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-enforcer-plugin</artifactId>
                <version>1.4.1</version>
                <executions>
                    <execution>
                        <id>enforce-maven</id>
                        <goals>
                            <goal>enforce</goal>
                        </goals>
                        <configuration>
                            <rules>
                                <requireMavenVersion>
                                    <version>[3.5,)</version>
                                </requireMavenVersion>
                            </rules>
                        </configuration>
                    </execution>
                </executions>
            </plugin>

            <!-- Enable license header check. -->
            <plugin>
                <groupId>com.mycila</groupId>
                <artifactId>license-maven-plugin</artifactId>
                <version>${license.maven.plugin.version}</version>
                <configuration>
                    <failIfUnknown>true</failIfUnknown>
                    <mapping>
                        <ant>XML_STYLE</ant>
                        <exsd>XML_STYLE</exsd>
                        <ini>SCRIPT_STYLE</ini>
                        <mwe2>JAVADOC_STYLE</mwe2>
                        <product>XML_STYLE</product>
                        <types>JAVADOC_STYLE</types>
                        <xtend>JAVADOC_STYLE</xtend>
                        <xtext>JAVADOC_STYLE</xtext>
                        <qvto>JAVADOC_STYLE</qvto>
                        <melk>JAVADOC_STYLE</melk>
                        <bat>BATCH</bat>
                        <cmd>BATCH</cmd>
                        <plantuml>APOSTROPHE_STYLE</plantuml>
                        <asciidoc>ASCIIDOC_STYLE</asciidoc>
                        <adoc>ASCIIDOC_STYLE</adoc>
                        <csv>SCRIPT_STYLE</csv>
                        <Jenkinsfile>DOUBLESLASH_STYLE</Jenkinsfile>
                        <etl>JAVADOC_STYLE</etl>
                        <etf>SCRIPT_STYLE</etf>
                        <mappings>SCRIPT_STYLE</mappings>
                    </mapping>
                    <licenseSets>
                        <licenseSet>
                            <header>${session.executionRootDirectory}/releng/org.eclipse.trace4cps.license/license-header.txt</header>
                            <useDefaultExcludes>true</useDefaultExcludes>
                            <excludes>
                                <exclude>src-gen/**</exclude>
                                <exclude>xtend-gen/**</exclude>
                                <exclude>**/*.e4xmi</exclude>
                                <exclude>**/*.ecore</exclude>
                                <exclude>**/*.genmodel</exclude>
                                <exclude>**/*.launch</exclude>
                                <exclude>**/*.setup</exclude>
                                <exclude>**/*.target</exclude>
                                <exclude>**/*.txt</exclude>
                                <exclude>.checkstyle</exclude>
                                <exclude>.META-INF_MANIFEST.MF</exclude>
                                <exclude>.polyglot.*</exclude>
                                <exclude>.Xauthority*</exclude>
                                <exclude>checkstyle.xml</exclude>
                                <exclude>plugin.xml_gen</exclude>
                                <exclude>pom.tycho</exclude>
                            </excludes>
                        </licenseSet>
                    </licenseSets>
                </configuration>
                <executions>
                    <!-- use the cmd line `./build.sh license:format` to add headers -->
                    <execution>
                        <id>checkLicenseHeaders</id>
                        <goals>
                            <goal>check</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>

            <!-- Enable Tycho. -->
            <plugin>
                <groupId>org.eclipse.tycho</groupId>
                <artifactId>tycho-maven-plugin</artifactId>
                <version>${tycho.version}</version>

                <!-- Enable extensions. -->
                <extensions>true</extensions>
            </plugin>

            <!-- Configure Java compiler. -->
            <plugin>
                <groupId>org.eclipse.tycho</groupId>
                <artifactId>tycho-compiler-plugin</artifactId>
                <version>${tycho.version}</version>
            </plugin>

            <!-- Configure packaging of JARs. -->
            <plugin>
                <groupId>org.eclipse.tycho</groupId>
                <artifactId>tycho-packaging-plugin</artifactId>
                <version>${tycho.version}</version>

                <configuration>
                    <!-- Use property-based fixed qualifier for all plugins/features/etc. -->
                    <forceContextQualifier>${trace4cps.version.qualifier}</forceContextQualifier>

                    <!-- Disable Maven descriptors in JAR bundles. -->
                    <archive>
                        <addMavenDescriptor>false</addMavenDescriptor>
                    </archive>
                </configuration>
            </plugin>

            <!-- Enable and configure target platform. -->
            <plugin>
                <groupId>org.eclipse.tycho</groupId>
                <artifactId>target-platform-configuration</artifactId>
                <version>${tycho.version}</version>
                <configuration>
                    <!-- Configure target platform file. -->
                    <target>
                        <artifact>
                            <groupId>org.eclipse.trace4cps</groupId>
                            <artifactId>org.eclipse.trace4cps.target</artifactId>
                            <version>0.2.0-SNAPSHOT</version>
                        </artifact>
                    </target>

                    <!--
                        Configure environments.
                        Determines which environment specific bundles will be in target platform.
                        Also determines the products to build.
                    -->
                    <environments>
                        <environment>
                            <os>linux</os>
                            <ws>gtk</ws>
                            <arch>x86_64</arch>
                        </environment>
                        <environment>
                            <os>win32</os>
                            <ws>win32</ws>
                            <arch>x86_64</arch>
                        </environment>
                        <environment>
                            <os>macosx</os>
                            <ws>cocoa</ws>
                            <arch>x86_64</arch>
                        </environment>
                    </environments>
                </configuration>
            </plugin>

            <!-- Enable unit testing. -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>3.0.0-M5</version>
                <dependencies>
                    <dependency>
                        <groupId>org.apache.maven.surefire</groupId>
                        <artifactId>surefire-junit47</artifactId>
                        <version>3.0.0-M5</version>
                    </dependency>
                </dependencies>
            </plugin>

            <!-- Enable Xtend compiler. -->
            <plugin>
                <groupId>org.eclipse.xtend</groupId>
                <artifactId>xtend-maven-plugin</artifactId>
                <version>${xtext.version}</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>compile</goal>
                            <goal>xtend-install-debug-info</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <outputDirectory>${project.basedir}/xtend-gen</outputDirectory>
                    <testOutputDirectory>${project.basedir}/xtend-gen</testOutputDirectory>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-clean-plugin</artifactId>
                <version>3.1.0</version>
                <configuration>
                    <filesets combine.children="append">
                        <fileset>
                            <directory>${project.basedir}/xtend-gen</directory>
                            <includes>
                                <include>**/*</include>
                            </includes>
                        </fileset>
                    </filesets>
                </configuration>
            </plugin>
        </plugins>

        <pluginManagement>
            <plugins>
                <plugin>
                    <artifactId>maven-assembly-plugin</artifactId>
                    <version>2.6</version>
                </plugin>

                <plugin>
                    <groupId>org.eclipse.tycho</groupId>
                    <artifactId>tycho-versions-plugin</artifactId>
                    <version>${tycho.version}</version>
                </plugin>

                <plugin>
                    <groupId>org.eclipse.tycho</groupId>
                    <artifactId>tycho-surefire-plugin</artifactId>
                    <version>${tycho.version}</version>
                </plugin>

                <!-- AsciiDoctor documentation generation configuration. -->
                <plugin>
                    <groupId>org.asciidoctor</groupId>
                    <artifactId>asciidoctor-maven-plugin</artifactId>
                    <version>${asciidoctor.maven.plugin.version}</version>
                    <dependencies>
                        <dependency>
                            <groupId>org.asciidoctor</groupId>
                            <artifactId>asciidoctorj</artifactId>
                            <version>${asciidoctorj.version}</version>
                        </dependency>
                        <dependency>
                            <groupId>org.asciidoctor</groupId>
                            <artifactId>asciidoctorj-diagram</artifactId>
                            <version>${asciidoctorj.diagram.version}</version>
                        </dependency>
                        <dependency>
                            <groupId>org.asciidoctor</groupId>
                            <artifactId>asciidoctorj-pdf</artifactId>
                            <version>${asciidoctorj.pdf.version}</version>
                        </dependency>
                        <dependency>
                            <groupId>org.jruby</groupId>
                            <artifactId>jruby-complete</artifactId>
                            <version>${jruby.version}</version>
                        </dependency>
                    </dependencies>
                    <configuration>
                        <requires>
                            <require>asciidoctor-diagram</require>
                        </requires>
                        <!-- Attributes common to all documents. -->
                        <attributes>
                            <baseDir>${project.basedir}</baseDir>
                            <attribute-missing>error</attribute-missing>
                            <attribute-undefined>error</attribute-undefined>
                            <revnumber>${trace4cps.version.enduser}</revnumber>
                            <last-update-label>false</last-update-label>
                            <asciimath />
                        </attributes>
                    </configuration>
                </plugin>

                <!--
                    Eclipse Dash, license check, configuration.
                    Plugin executed manually, not during Maven build.
                    Can't integrate into Maven build yet, as won't fail.
                    See https://github.com/eclipse/dash-licenses/issues/70
                -->
                <plugin>
                    <groupId>org.eclipse.dash</groupId>
                    <artifactId>license-tool-plugin</artifactId>
                    <version>${eclipse.dash.license.tool.version}</version>
                    <configuration>
                        <projectId>technology.trace4cps</projectId>
                    </configuration>
                </plugin>
            </plugins>
        </pluginManagement>
    </build>

    <profiles>
        <!-- Enable Checkstyle code analysis. -->
        <profile>
            <id>checkstyle-check</id>
            <activation>
                <file>
                    <exists>${basedir}/.checkstyle</exists>
                </file>
            </activation>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-checkstyle-plugin</artifactId>
                        <version>${maven.checkstyle.version}</version>
                        <dependencies>
                            <dependency>
                                <groupId>com.puppycrawl.tools</groupId>
                                <artifactId>checkstyle</artifactId>
                                <version>${checkstyle.version}</version>
                            </dependency>
                        </dependencies>
                        <executions>
                            <execution>
                                <id>checkstyle-validate</id>
                                <phase>validate</phase>
                                <configuration>
                                    <configLocation>checkstyle.xml</configLocation>
                                    <consoleOutput>false</consoleOutput>
                                    <failOnViolation>true</failOnViolation>
                                    <violationSeverity>info</violationSeverity>
                                    <sourceDirectories>
                                        <sourceDirectory>${project.basedir}/src</sourceDirectory>
                                    </sourceDirectories>
                                </configuration>
                                <goals>
                                    <goal>check</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>

        <!-- Add default about.html to (source) bundles if not already present. -->
        <profile>
            <id>add-plugin-default-abouts</id>
            <activation>
                <file>
                    <missing>${basedir}/about.html</missing>
                </file>
            </activation>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.eclipse.tycho</groupId>
                        <artifactId>tycho-packaging-plugin</artifactId>
                        <version>${tycho.version}</version>
                        <configuration>
                            <additionalFileSets>
                                <fileSet>
                                    <directory>${session.executionRootDirectory}/releng/org.eclipse.trace4cps.branding</directory>
                                    <includes>
                                        <include>about.html</include>
                                    </includes>
                                </fileSet>
                            </additionalFileSets>
                        </configuration>
                    </plugin>

                    <plugin>
                        <groupId>org.eclipse.tycho</groupId>
                        <artifactId>tycho-source-plugin</artifactId>
                        <version>${tycho.version}</version>
                        <configuration>
                            <additionalFileSets>
                                <fileSet>
                                    <directory>${session.executionRootDirectory}/releng/org.eclipse.trace4cps.branding</directory>
                                    <includes>
                                        <include>about.html</include>
                                    </includes>
                                </fileSet>
                            </additionalFileSets>
                        </configuration>
                    </plugin>
                </plugins>
            </build>
        </profile>

        <profile>
            <id>about-mappings</id>
            <activation>
                <file>
                    <exists>${basedir}/about.mappings</exists>
                </file>
            </activation>
            <build>
                <resources>
                    <resource>
                        <directory>${basedir}</directory>
                        <filtering>true</filtering>
                        <includes>
                            <include>about.mappings</include>
                        </includes>
                    </resource>
                </resources>
                <plugins>
                    <plugin>
                        <groupId>org.eclipse.tycho</groupId>
                        <artifactId>tycho-packaging-plugin</artifactId>
                        <version>${tycho.version}</version>
                        <configuration>
                            <additionalFileSets>
                                <fileSet>
                                    <directory>${project.build.outputDirectory}</directory>
                                    <includes>
                                        <include>about.mappings</include>
                                    </includes>
                                </fileSet>
                            </additionalFileSets>
                        </configuration>
                    </plugin>
                </plugins>
            </build>
        </profile>

        <profile>
            <id>create-plugin-source</id>
            <activation>
                <file>
                    <exists>${basedir}/META-INF/MANIFEST.MF</exists>
                </file>
            </activation>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.eclipse.tycho</groupId>
                        <artifactId>tycho-source-plugin</artifactId>
                        <version>${tycho.version}</version>
                        <executions>
                            <execution>
                                <id>plugin-source</id>
                                <goals>
                                    <goal>plugin-source</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>

        <profile>
            <id>create-feature-source</id>
            <activation>
                <file>
                    <exists>${basedir}/feature.xml</exists>
                </file>
            </activation>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.eclipse.tycho.extras</groupId>
                        <artifactId>tycho-source-feature-plugin</artifactId>
                        <version>${tycho.extras.version}</version>
                        <configuration>
                            <skip>${source.feature.skip}</skip>
                            <missingSourcesAction>FAIL</missingSourcesAction>
                        </configuration>
                        <executions>
                            <execution>
                                <id>source-feature</id>
                                <goals>
                                    <goal>source-feature</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                    <plugin>
                        <groupId>org.eclipse.tycho</groupId>
                        <artifactId>tycho-p2-plugin</artifactId>
                        <version>${tycho.version}</version>
                        <!--
                            The following is needed for generated source features that are part of an update site.
                            Tycho will warn about this if not configured.
                        -->
                        <executions>
                            <execution>
                                <!-- Don't attach (default) metadata before the "generate-source-feature" execution. -->
                                <id>default-p2-metadata-default</id>
                                <configuration>
                                    <attachP2Metadata>false</attachP2Metadata>
                                </configuration>
                            </execution>
                            <execution>
                                <!-- Do attach metadata after the "generate-source-feature" execution. -->
                                <id>attach-p2-metadata</id>
                                <phase>package</phase>
                                <goals>
                                    <goal>p2-metadata</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>

        <!-- Signing. -->
        <profile>
            <id>sign</id>
            <build>
                <plugins>
                    <!-- Sign JARs. -->
                    <plugin>
                        <groupId>org.eclipse.cbi.maven.plugins</groupId>
                        <artifactId>eclipse-jarsigner-plugin</artifactId>
                        <version>${eclipse.cbi.version}</version>
                        <executions>
                            <execution>
                                <id>sign</id>
                                <phase>verify</phase>
                                <goals>
                                    <goal>sign</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>

        <!-- This profile prevents errors for m2eclipse, only when Eclipse is active. -->
        <profile>
            <id>eclipse-m2e</id>
            <activation>
                <property>
                    <name>m2e.version</name>
                </property>
            </activation>
            <build>
                <pluginManagement>
                    <plugins>
                        <!--
                            This configuration prevents errors for m2eclipse.
                            It affects m2eclipse settings only. It has no influence on the Maven build itself.
                            It prevents the following error:
                            "Plugin execution not covered by lifecycle configuration:
                            org.eclipse.tycho:target-platform-configuration:2.5.0:target-platform
                            (execution: default-target-platform, phase: initialize)"
                            Note that according to the documentation of the Tycho Maven plugin:
                            "This mojo is actually not executable, and is only meant to host configuration."
                            It is thus safe to set the action to 'ignore' using lifecycle mapping metadata configuration.
                        -->
                        <plugin>
                            <groupId>org.eclipse.m2e</groupId>
                            <artifactId>lifecycle-mapping</artifactId>
                            <version>1.0.0</version>
                            <configuration>
                                <lifecycleMappingMetadata>
                                    <pluginExecutions>
                                        <pluginExecution>
                                            <pluginExecutionFilter>
                                                <groupId>org.eclipse.tycho</groupId>
                                                <artifactId>target-platform-configuration</artifactId>
                                                <versionRange>[1.7.0,)</versionRange>
                                                <goals>
                                                    <goal>target-platform</goal>
                                                </goals>
                                            </pluginExecutionFilter>
                                            <action>
                                                <ignore></ignore>
                                            </action>
                                        </pluginExecution>
                                        <pluginExecution>
                                            <pluginExecutionFilter>
                                                <groupId>org.apache.maven.plugins</groupId>
                                                <artifactId>maven-checkstyle-plugin</artifactId>
                                                <versionRange>[3.1.2,)</versionRange>
                                                <goals>
                                                    <goal>check</goal>
                                                </goals>
                                            </pluginExecutionFilter>
                                            <action>
                                                <ignore></ignore>
                                            </action>
                                        </pluginExecution>
                                    </pluginExecutions>
                                </lifecycleMappingMetadata>
                            </configuration>
                        </plugin>
                    </plugins>
                </pluginManagement>
            </build>
        </profile>
    </profiles>
</project>
