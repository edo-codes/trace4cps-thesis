<!--

    Copyright (c) 2021 Contributors to the Eclipse Foundation

    This program and the accompanying materials are made
    available under the terms of the Eclipse Public License 2.0
    which is available at https://www.eclipse.org/legal/epl-2.0/

    SPDX-License-Identifier: EPL-2.0

-->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>org.eclipse.trace4cps</groupId>
        <artifactId>org.eclipse.trace4cps.root</artifactId>
        <version>0.2.0-SNAPSHOT</version>
        <relativePath>../../</relativePath>
    </parent>

    <name>TRACE4CPS Runtime Verification</name>
    <artifactId>org.eclipse.trace4cps.tl</artifactId>
    <packaging>eclipse-plugin</packaging>

    <build>
        <pluginManagement>
            <plugins>
                <!--
                    This plugin's configuration is used to store Eclipse m2e settings only.
                    It has no influence on the Maven build itself.
                -->
                <plugin>
                    <groupId>org.eclipse.m2e</groupId>
                    <artifactId>lifecycle-mapping</artifactId>
                    <version>1.0.0</version>
                    <configuration>
                        <lifecycleMappingMetadata>
                            <pluginExecutions>
                                <pluginExecution>
                                    <pluginExecutionFilter>
                                        <groupId>org.codehaus.mojo</groupId>
                                        <artifactId>exec-maven-plugin</artifactId>
                                        <versionRange>[1.6.0,)</versionRange>
                                        <goals>
                                            <goal>java</goal>
                                        </goals>
                                    </pluginExecutionFilter>
                                    <action>
                                        <ignore></ignore>
                                    </action>
                                </pluginExecution>
                            </pluginExecutions>
                        </lifecycleMappingMetadata>
                    </configuration>
                </plugin>
            </plugins>
        </pluginManagement>
    </build>

    <profiles>
        <profile>
            <id>mwe2Launcher</id>
            <activation>
                <activeByDefault>true</activeByDefault>
            </activation>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.codehaus.mojo</groupId>
                        <artifactId>exec-maven-plugin</artifactId>
                        <version>1.6.0</version>
                        <executions>
                            <execution>
                                <id>mwe2Launcher</id>
                                <phase>generate-sources</phase>
                                <goals>
                                    <goal>java</goal>
                                </goals>
                            </execution>
                        </executions>
                        <configuration>
                            <mainClass>org.eclipse.emf.mwe2.launch.runtime.Mwe2Launcher</mainClass>
                            <arguments>
                                <argument>/${project.basedir}/src/org/eclipse/trace4cps/tl/GenerateEtl.mwe2</argument>
                                <argument>-p</argument>
                                <argument>rootPath=/${project.basedir}/..</argument>
                            </arguments>
                            <classpathScope>compile</classpathScope>
                            <includePluginDependencies>true</includePluginDependencies>
                            <cleanupDaemonThreads>false</cleanupDaemonThreads><!-- see https://bugs.eclipse.org/bugs/show_bug.cgi?id=475098#c3 -->
                        </configuration>
                        <dependencies>
                            <dependency>
                                <groupId>org.eclipse.emf</groupId>
                                <artifactId>org.eclipse.emf.mwe2.launch</artifactId>
                                <version>${mwe2.version}</version>
                            </dependency>
                            <dependency>
                                <groupId>org.eclipse.xtext</groupId>
                                <artifactId>org.eclipse.xtext.common.types</artifactId>
                                <version>${xtext.version}</version>
                            </dependency>
                            <dependency>
                                <groupId>org.eclipse.xtext</groupId>
                                <artifactId>org.eclipse.xtext.xtext.generator</artifactId>
                                <version>${xtext.version}</version>
                                <!-- Workaround for https://github.com/eclipse/xtext/issues/1976 and -->
                                <!-- Workaround for https://github.com/eclipse-equinox/equinox.bundles/issues/54 -->
                                <exclusions>
                                    <exclusion>
                                        <groupId>org.eclipse.platform</groupId>
                                        <artifactId>*</artifactId>
                                    </exclusion>
                                </exclusions>
                            </dependency>
                            <dependency>
                                <groupId>org.eclipse.xtext</groupId>
                                <artifactId>org.eclipse.xtext.xbase</artifactId>
                                <version>${xtext.version}</version>
                            </dependency>
                            <dependency>
                                <groupId>org.eclipse.xtext</groupId>
                                <artifactId>xtext-antlr-generator</artifactId>
                                <version>2.1.1</version>
                            </dependency>
                        </dependencies>
                    </plugin>

                    <plugin>
                        <groupId>org.eclipse.xtend</groupId>
                        <artifactId>xtend-maven-plugin</artifactId>
                    </plugin>

                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-clean-plugin</artifactId>
                        <configuration>
                            <filesets combine.children="append">
                                <fileset>
                                    <directory>${project.basedir}/../org.eclipse.trace4cps.tl/src-gen/</directory>
                                    <includes>
                                        <include>**/*</include>
                                    </includes>
                                </fileset>
                                <fileset>
                                    <directory>${project.basedir}/../org.eclipse.trace4cps.tl.tests/src-gen/</directory>
                                    <includes>
                                        <include>**/*</include>
                                    </includes>
                                </fileset>
                                <fileset>
                                    <directory>${project.basedir}/../org.eclipse.trace4cps.tl.ide/src-gen/</directory>
                                    <includes>
                                        <include>**/*</include>
                                    </includes>
                                </fileset>
                                <fileset>
                                    <directory>${project.basedir}/../org.eclipse.trace4cps.tl.ui/src-gen/</directory>
                                    <includes>
                                        <include>**/*</include>
                                    </includes>
                                </fileset>
                                <fileset>
                                    <directory>${project.basedir}/../org.eclipse.trace4cps.tl.ui.tests/src-gen/</directory>
                                    <includes>
                                        <include>**/*</include>
                                    </includes>
                                </fileset>
                                <fileset>
                                    <directory>${project.basedir}/model/generated/</directory>
                                </fileset>
                            </filesets>
                        </configuration>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>
</project>
