/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.trace4cps.tl.formatting2

import org.eclipse.trace4cps.tl.etl.EtlModel
import org.eclipse.xtext.formatting2.AbstractFormatter2
import org.eclipse.xtext.formatting2.IFormattableDocument

class EtlFormatter extends AbstractFormatter2 {

    def dispatch void format(EtlModel etlModel, extension IFormattableDocument document) {
        // TODO: format HiddenRegions around keywords, attributes, cross references, etc. 
        for (_signal : etlModel.elements) {
            _signal.format
            _signal.append[newLines = 2]
        }
    }

    // TODO: implement for Def, Check, Formula, Globally0, Finally0, Globally1, Finally1, IfThenFormula, AndFormula, 
    //       OrFormula, Until0, Until1, NotFormula, MTLleaf, KeyVal, Interval
//    def dispatch void format(Def _def, extension IFormattableDocument document) {
//        // TODO: format HiddenRegions around keywords, attributes, cross references, etc. 
//        _def.formula.format
//    }
}
