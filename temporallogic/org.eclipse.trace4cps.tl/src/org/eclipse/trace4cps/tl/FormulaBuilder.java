/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.tl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.eclipse.trace4cps.analysis.mtl.AtomicProposition;
import org.eclipse.trace4cps.analysis.mtl.DefaultAtomicProposition;
import org.eclipse.trace4cps.analysis.mtl.MtlBuilder;
import org.eclipse.trace4cps.analysis.mtl.MtlFormula;
import org.eclipse.trace4cps.analysis.mtl.impl.MTLtrue;
import org.eclipse.trace4cps.analysis.signal.impl.PsopHelper;
import org.eclipse.trace4cps.analysis.stl.StlBuilder;
import org.eclipse.trace4cps.analysis.stl.StlFormula;
import org.eclipse.trace4cps.core.ClaimEventType;
import org.eclipse.trace4cps.core.IPsop;
import org.eclipse.trace4cps.core.ITrace;
import org.eclipse.trace4cps.core.impl.Interval;
import org.eclipse.trace4cps.tl.etl.AndOrFormula;
import org.eclipse.trace4cps.tl.etl.ApFormula;
import org.eclipse.trace4cps.tl.etl.AttributeFilter;
import org.eclipse.trace4cps.tl.etl.Check;
import org.eclipse.trace4cps.tl.etl.Def;
import org.eclipse.trace4cps.tl.etl.EtlModel;
import org.eclipse.trace4cps.tl.etl.FinallyFormula;
import org.eclipse.trace4cps.tl.etl.FinallyUntimedFormula;
import org.eclipse.trace4cps.tl.etl.Formula;
import org.eclipse.trace4cps.tl.etl.GloballyFormula;
import org.eclipse.trace4cps.tl.etl.GloballyUntimedFormula;
import org.eclipse.trace4cps.tl.etl.IdString;
import org.eclipse.trace4cps.tl.etl.IfThenFormula;
import org.eclipse.trace4cps.tl.etl.KeyVal;
import org.eclipse.trace4cps.tl.etl.MtlAp;
import org.eclipse.trace4cps.tl.etl.MtlApEnd;
import org.eclipse.trace4cps.tl.etl.MtlApStart;
import org.eclipse.trace4cps.tl.etl.NextFormula;
import org.eclipse.trace4cps.tl.etl.NotFormula;
import org.eclipse.trace4cps.tl.etl.ReferenceFormula;
import org.eclipse.trace4cps.tl.etl.SignalDef;
import org.eclipse.trace4cps.tl.etl.StlAp;
import org.eclipse.trace4cps.tl.etl.StlApDeriv;
import org.eclipse.trace4cps.tl.etl.TopLevelModelElement;
import org.eclipse.trace4cps.tl.etl.TrueFormula;
import org.eclipse.trace4cps.tl.etl.UntilFormula;
import org.eclipse.trace4cps.tl.etl.UntilUntimedFormula;

/**
 * This type is responsible from converting a specification in the DSL to a set of {@link MtlFormula} instances. These
 * are stored in the returned {@link VerificationResult}.
 */
public class FormulaBuilder {
    private final ITrace trace;

    private final EtlModel m;

    private final VerificationResult result;

    private final Map<String, IPsop> signalMap;

    private String currentCheck;

    public FormulaBuilder(ITrace trace, Map<String, IPsop> signalMap, EtlModel m) throws FormulaBuilderException {
        this.trace = trace;
        this.m = m;
        this.result = new VerificationResult();
        this.signalMap = signalMap;
    }

    public static <T extends TopLevelModelElement> List<T> get(EtlModel model, Class<T> clazz) {
        List<T> r = new ArrayList<>();
        for (TopLevelModelElement e: model.getElements()) {
            if (clazz.isAssignableFrom(e.getClass())) {
                r.add(clazz.cast(e));
            }
        }
        return r;
    }

    /**
     * Creates a {@link VerificationResult} instance that contains the MTL formulas from the specification given on
     * construction. It distinguishes signals, defs and checks. The result of the verification itself is not filled in
     * yet.
     */
    public VerificationResult create() throws FormulaBuilderException {
        for (Check chk: get(m, Check.class)) {
            String name = chk.getName();
            Formula etl = chk.getFormula();
            if (chk.getVar() != null) {
                for (int i = chk.getLb(); i <= chk.getUb(); i++) {
                    this.currentCheck = name + "(" + Integer.toString(i) + ")";
                    MtlFormula f = create(etl, i, trace.getTimeUnit(), signalMap, result);
                    result.add(name, f, i);
                }
            } else {
                this.currentCheck = name;
                MtlFormula f = create(etl, null, trace.getTimeUnit(), signalMap, result);
                result.add(name, f, true);
            }
        }
        return result;
    }

    public static Map<String, String> toMap(AttributeFilter kvs, Integer i) {
        Map<String, String> filter = new HashMap<String, String>();
        for (KeyVal kv: kvs.getKeyVals()) {
            filter.put(getIdString(kv.getAtt(), i), getIdString(kv.getVal(), i));
        }
        return filter;
    }

    public static String getIdString(IdString ids, Integer i) {
        StringBuilder b = new StringBuilder();
        if (ids.getLeft() != null) {
            b.append(ids.getLeft());
        }
        if (ids.getId() != null) {
            b.append(i.toString());
        }
        if (ids.getRight() != null) {
            b.append(ids.getRight());
        }
        return b.toString();
    }

    /**
     * Only works for non-parameterized formulas!
     *
     */
    public MtlFormula create(Formula f, ITrace trace, List<SignalDef> signalDefs, VerificationResult result)
            throws FormulaBuilderException
    {
        Map<String, IPsop> signalMap = FormulaHelper.createSignalMap(trace, signalDefs, true);
        return create(f, null, trace.getTimeUnit(), signalMap, result);
    }

    public MtlFormula create(Formula f, Integer qValue, TimeUnit traceTu, Map<String, IPsop> signalMap,
            VerificationResult result) throws FormulaBuilderException
    {
        MtlFormula mf;
        if (f instanceof ReferenceFormula) {
            mf = createRef((ReferenceFormula)f, qValue, traceTu, signalMap, result);
        } else if (f instanceof ApFormula) {
            mf = createAp((ApFormula)f, qValue, traceTu, signalMap, result);
        } else if (f instanceof AndOrFormula) {
            mf = createAndOr((AndOrFormula)f, qValue, traceTu, signalMap, result);
        } else if (f instanceof IfThenFormula) {
            mf = createImply((IfThenFormula)f, qValue, traceTu, signalMap, result);
        } else if (f instanceof NotFormula) {
            mf = createNot((NotFormula)f, qValue, traceTu, signalMap, result);
        } else if (f instanceof NextFormula) {
            mf = createNext((NextFormula)f, qValue, traceTu, signalMap, result);
        } else if (f instanceof FinallyUntimedFormula) {
            mf = createF0((FinallyUntimedFormula)f, qValue, traceTu, signalMap, result);
        } else if (f instanceof FinallyFormula) {
            mf = createF1((FinallyFormula)f, qValue, traceTu, signalMap, result);
        } else if (f instanceof GloballyUntimedFormula) {
            mf = createG0((GloballyUntimedFormula)f, qValue, traceTu, signalMap, result);
        } else if (f instanceof GloballyFormula) {
            mf = createG1((GloballyFormula)f, qValue, traceTu, signalMap, result);
        } else if (f instanceof UntilUntimedFormula) {
            mf = createU0((UntilUntimedFormula)f, qValue, traceTu, signalMap, result);
        } else if (f instanceof UntilFormula) {
            mf = createU1((UntilFormula)f, qValue, traceTu, signalMap, result);
        } else if (f instanceof TrueFormula) {
            mf = createTrue((TrueFormula)f, qValue, traceTu, signalMap, result);
        } else {
            throw new IllegalStateException("Failed to create formula for " + f);
        }
        result.addEtl(f, mf, currentCheck);
        return mf;
    }

    private MtlFormula createTrue(TrueFormula t, Integer qValue, TimeUnit traceTu, Map<String, IPsop> signalMap,
            VerificationResult result) throws FormulaBuilderException
    {
        return new MTLtrue();
    }

    private MtlFormula createAndOr(AndOrFormula ao, Integer qValue, TimeUnit traceTu, Map<String, IPsop> signalMap,
            VerificationResult result) throws FormulaBuilderException
    {
        switch (ao.getOp()) {
            case AND:
                return createAnd(ao, qValue, traceTu, signalMap, result);
            case OR:
                return createOr(ao, qValue, traceTu, signalMap, result);
            default:
                throw new IllegalStateException();
        }
    }

    private MtlFormula createAp(ApFormula ap, Integer qValue, TimeUnit traceTu, Map<String, IPsop> signalMap,
            VerificationResult result) throws FormulaBuilderException
    {
        if (ap.getMtlAP() != null) {
            return createMTL(ap.getMtlAP(), qValue);
        } else {
            return createSTL(ap.getStlAP(), qValue, traceTu, signalMap, result);
        }
    }

    private MtlFormula createRef(ReferenceFormula f, Integer qValue, TimeUnit traceTu, Map<String, IPsop> signalMap,
            VerificationResult result) throws FormulaBuilderException
    {
        Def def = f.getDef();
        Formula etl = def.getFormula();
        int addend = f.getVal();
        String name = def.getName();
        MtlFormula defFormula = create(etl, qValue == null ? null : qValue + addend, traceTu, signalMap, result);
        if (qValue != null) {
            name += "(" + (qValue + addend) + ")";
        }
        if (result != null && !result.contains(defFormula)) {
            result.add(name, defFormula, false);
        }
        return defFormula;
    }

    private AtomicProposition createMTL(MtlAp f, Integer qValue) throws FormulaBuilderException {
        Map<String, String> props = new HashMap<String, String>();
        ClaimEventType type = f instanceof MtlApStart ? ClaimEventType.START
                : f instanceof MtlApEnd ? ClaimEventType.END : null;
        for (KeyVal kv: f.getFilter().getKeyVals()) {
            String key = getIdString(kv.getAtt(), qValue);
            String val = getIdString(kv.getVal(), qValue);
            props.put(key, val);
            result.addAtom(currentCheck, qValue, type, key, val);
        }
        return new DefaultAtomicProposition(type, props);
    }

    private MtlFormula createSTL(StlAp f, Integer qValue, TimeUnit traceTu, Map<String, IPsop> signalMap,
            VerificationResult result) throws FormulaBuilderException
    {
        IPsop p = signalMap.get(f.getRef().getName());
        if (f instanceof StlApDeriv) {
            p = PsopHelper.createDerivativeOf(p);
        }
        double val = f.getVal();
        switch (f.getCompOp()) {
            case EQ:
                return StlBuilder.EQ(p, val);
            case GE:
                return StlBuilder.GEQ(p, val);
            case LE:
                return StlBuilder.LEQ(p, val);
            default:
                throw new IllegalStateException();
        }
    }

    private MtlFormula createAnd(AndOrFormula f, Integer qValue, TimeUnit traceTu, Map<String, IPsop> signalMap,
            VerificationResult result) throws FormulaBuilderException
    {
        MtlFormula l = create(f.getLeft(), qValue, traceTu, signalMap, result);
        MtlFormula r = create(f.getRight(), qValue, traceTu, signalMap, result);
        if (l instanceof StlFormula && r instanceof StlFormula) {
            return StlBuilder.AND((StlFormula)l, (StlFormula)r);
        } else {
            return MtlBuilder.AND(l, r);
        }
    }

    private MtlFormula createOr(AndOrFormula f, Integer qValue, TimeUnit traceTu, Map<String, IPsop> signalMap,
            VerificationResult result) throws FormulaBuilderException
    {
        MtlFormula l = create(f.getLeft(), qValue, traceTu, signalMap, result);
        MtlFormula r = create(f.getRight(), qValue, traceTu, signalMap, result);
        if (l instanceof StlFormula && r instanceof StlFormula) {
            return StlBuilder.OR((StlFormula)l, (StlFormula)r);
        } else {
            return MtlBuilder.OR(l, r);
        }
    }

    private MtlFormula createImply(IfThenFormula f, Integer qValue, TimeUnit traceTu, Map<String, IPsop> signalMap,
            VerificationResult result) throws FormulaBuilderException
    {
        MtlFormula l = create(f.getLeft(), qValue, traceTu, signalMap, result);
        MtlFormula r = create(f.getRight(), qValue, traceTu, signalMap, result);
        if (l instanceof StlFormula && r instanceof StlFormula) {
            return StlBuilder.IMPLY((StlFormula)l, (StlFormula)r);
        } else {
            return MtlBuilder.IMPLY(l, r);
        }
    }

    private MtlFormula createNot(NotFormula f, Integer qValue, TimeUnit traceTu, Map<String, IPsop> signalMap,
            VerificationResult result) throws FormulaBuilderException
    {
        MtlFormula f2 = create(f.getFormula(), qValue, traceTu, signalMap, result);
        if (f2 instanceof StlFormula) {
            return StlBuilder.NOT((StlFormula)f2);
        } else {
            return MtlBuilder.NOT(f2);
        }
    }

    private MtlFormula createNext(NextFormula f, Integer qValue, TimeUnit traceTu, Map<String, IPsop> signalMap,
            VerificationResult result) throws FormulaBuilderException
    {
        MtlFormula f2 = create(f.getFormula(), qValue, traceTu, signalMap, result);
        // next not supported by STL algorithm
        return MtlBuilder.X(f2);
    }

    private MtlFormula createF0(FinallyUntimedFormula f, Integer qValue, TimeUnit traceTu, Map<String, IPsop> signalMap,
            VerificationResult result) throws FormulaBuilderException
    {
        MtlFormula f2 = create(f.getFormula(), qValue, traceTu, signalMap, result);
        if (f2 instanceof StlFormula) {
            return StlBuilder.F((StlFormula)f2);
        } else {
            return MtlBuilder.F(f2);
        }
    }

    private MtlFormula createF1(FinallyFormula f, Integer qValue, TimeUnit traceTu, Map<String, IPsop> signalMap,
            VerificationResult result) throws FormulaBuilderException
    {
        MtlFormula f2 = create(f.getFormula(), qValue, traceTu, signalMap, result);
        Interval i = FormulaHelper.from(traceTu, f.getInterval());
        if (f2 instanceof StlFormula) {
            return StlBuilder.F((StlFormula)f2, i.lb().doubleValue(), i.ub().doubleValue());
        } else {
            return MtlBuilder.F(f2, i);
        }
    }

    private MtlFormula createG0(GloballyUntimedFormula f, Integer qValue, TimeUnit traceTu,
            Map<String, IPsop> signalMap, VerificationResult result) throws FormulaBuilderException
    {
        MtlFormula f2 = create(f.getFormula(), qValue, traceTu, signalMap, result);
        if (f2 instanceof StlFormula) {
            return StlBuilder.G((StlFormula)f2);
        } else {
            return MtlBuilder.G(f2);
        }
    }

    private MtlFormula createG1(GloballyFormula f, Integer qValue, TimeUnit traceTu, Map<String, IPsop> signalMap,
            VerificationResult result) throws FormulaBuilderException
    {
        MtlFormula f2 = create(f.getFormula(), qValue, traceTu, signalMap, result);
        Interval i = FormulaHelper.from(traceTu, f.getInterval());
        if (f2 instanceof StlFormula) {
            return StlBuilder.G((StlFormula)f2, i.lb().doubleValue(), i.ub().doubleValue());
        } else {
            return MtlBuilder.G(f2, i);
        }
    }

    private MtlFormula createU0(UntilUntimedFormula f, Integer qValue, TimeUnit traceTu, Map<String, IPsop> signalMap,
            VerificationResult result) throws FormulaBuilderException
    {
        MtlFormula left = create(f.getLeft(), qValue, traceTu, signalMap, result);
        MtlFormula right = create(f.getRight(), qValue, traceTu, signalMap, result);
        // general until not supported by STL algorithm
        return MtlBuilder.U(left, right, new Interval(0, false, Double.POSITIVE_INFINITY, true));
    }

    private MtlFormula createU1(UntilFormula f, Integer qValue, TimeUnit traceTu, Map<String, IPsop> signalMap,
            VerificationResult result) throws FormulaBuilderException
    {
        MtlFormula left = create(f.getLeft(), qValue, traceTu, signalMap, result);
        MtlFormula right = create(f.getRight(), qValue, traceTu, signalMap, result);
        // general until not supported by STL algorithm
        return MtlBuilder.U(left, right, FormulaHelper.from(traceTu, f.getInterval()));
    }
}
