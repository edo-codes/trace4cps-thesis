/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.view.verify;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.trace4cps.analysis.mtl.MtlFormula;
import org.eclipse.trace4cps.analysis.mtl.MtlUtil;
import org.eclipse.trace4cps.tl.FormulaHelper;
import org.eclipse.trace4cps.tl.VerificationResult;
import org.eclipse.trace4cps.tl.etl.Formula;
import org.eclipse.trace4cps.tl.etl.ReferenceFormula;
import org.eclipse.trace4cps.ui.view.TraceView;

public class FormulaNode implements IExplainableNode {
    private final TraceView view;

    private Formula etl;

    private MtlFormula phi;

    private MtlFormula check;

    private String checkName;

    private VerificationResult result;

    private List<FormulaNode> subFormulas = new ArrayList<>();

    boolean showMtl;

    public FormulaNode(Formula etl, MtlFormula phi, MtlFormula check, String checkName, VerificationResult res,
            TraceView v)
    {
        this(etl, phi, check, checkName, res, v, false);
    }

    public FormulaNode(Formula etl, MtlFormula phi, MtlFormula check, String checkName, VerificationResult res,
            TraceView v, boolean showMtl)
    {
        this.etl = etl;
        this.phi = phi;
        this.result = res;
        this.check = check;
        this.checkName = checkName;
        this.view = v;
        this.showMtl = showMtl;

        if (!showMtl) {
            FormulaHelper.getFormulaChildren(etl).forEach(subetl -> {
                MtlFormula subphi = res.getMtlFromEtl(subetl, checkName);
                FormulaNode fm = new FormulaNode(subetl, subphi, check, checkName, res, v);
                this.subFormulas.add(fm);
            });
        } else {
            phi.getChildren().forEach(subphi -> {
                FormulaNode fm = new FormulaNode(null, subphi, check, checkName, res, v, showMtl);
                this.subFormulas.add(fm);
            });
        }
    }

    @Override
    public TraceView getTraceView() {
        return view;
    }

    public String getLabel() {
        if (!this.showMtl) {
            return FormulaHelper.getFormulaLabel(etl);
        } else {
            return MtlUtil.getLabel(phi);
        }
    }

    @Override
    public MtlFormula getFormula() {
        return phi;
    }

    public Formula getEtl() {
        return etl;
    }

    /**
     * @return Returns the ETL formula, or if the formula is a reference to a def, the formula of the def
     */
    public Formula getInnerEtl() {
        if (etl instanceof ReferenceFormula) {
            return ((ReferenceFormula)etl).getDef().getFormula();
        } else {
            return etl;
        }
    }

    @Override
    public MtlFormula getCheck() {
        return check;
    }

    @Override
    public String getCheckName() {
        return checkName;
    }

    @Override
    public VerificationResult getResult() {
        return result;
    }

    public List<FormulaNode> getSubFormulas() {
        return subFormulas;
    }

    @Override
    public String toString() {
        return this.getLabel();
    }

    @Override
    public String getExplanationLabel() {
        return String.format("(%s) %s", checkName, getLabel());
    }
}
