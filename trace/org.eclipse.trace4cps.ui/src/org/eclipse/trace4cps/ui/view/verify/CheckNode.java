/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.view.verify;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.eclipse.trace4cps.analysis.mtl.InformativePrefix;
import org.eclipse.trace4cps.analysis.mtl.MtlFormula;
import org.eclipse.trace4cps.analysis.mtl.MtlResult;
import org.eclipse.trace4cps.tl.VerificationResult;
import org.eclipse.trace4cps.tl.etl.Formula;
import org.eclipse.trace4cps.ui.view.TraceView;

public class CheckNode implements IExplainableNode {
    private final TraceView view;

    private long checkTime;

    private VerificationResult result;

    private MtlFormula phi;

    private String name;

    private boolean isParameterized;

    private List<ParameterizedCheckNode> subChecks = new ArrayList<>();

    private FormulaNode formulaNode;

    public CheckNode(TraceView v) {
        view = v;
    }

    @Override
    public TraceView getTraceView() {
        return view;
    }

    public boolean isParameterized() {
        return isParameterized;
    }

    @Override
    public String getCheckName() {
        return name;
    }

    public long getCheckTime() {
        return checkTime;
    }

    @Override
    public MtlFormula getFormula() {
        if (isParameterized()) {
            throw new IllegalStateException();
        }
        return phi;
    }

    public List<ParameterizedCheckNode> getSubChecks() {
        if (!isParameterized()) {
            throw new IllegalStateException();
        }
        return subChecks;
    }

    public FormulaNode getFormulaNode() {
        return this.formulaNode;
    }

    public MtlResult getMTLresult() {
        if (isParameterized()) {
            throw new IllegalStateException();
        }
        return result.getResult(phi);
    }

    @Override
    public VerificationResult getResult() {
        if (isParameterized()) {
            throw new IllegalStateException();
        }
        return result;
    }

    public InformativePrefix informative() {
        if (isParameterized) {
            throw new IllegalStateException();
        }
        return result.getResult(phi).informative();
    }

    public void set(long timeStamp, MtlFormula phi, VerificationResult res, Formula etl) {
        checkTime = timeStamp;
        name = res.getName(phi);
        isParameterized = res.isQuantifiedCheck(phi);
        if (!res.isQuantifiedCheck(phi)) {
            this.phi = phi;
            result = res;
            this.formulaNode = new FormulaNode(etl, phi, phi, name, res, view);
        } else {
            subChecks.add(new ParameterizedCheckNode(phi, res, etl, view));
            Collections.sort(subChecks, Comparator.comparing(ParameterizedCheckNode::getQuantValue));
        }
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public String getExplanationLabel() {
        return formulaNode.getExplanationLabel();
    }

    @Override
    public MtlFormula getCheck() {
        return phi;
    }
}
