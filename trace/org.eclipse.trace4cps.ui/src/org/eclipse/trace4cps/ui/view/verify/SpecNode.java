/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.view.verify;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.eclipse.trace4cps.analysis.mtl.MtlFormula;
import org.eclipse.trace4cps.analysis.mtl.VerificationSemantics;
import org.eclipse.trace4cps.tl.VerificationResult;
import org.eclipse.trace4cps.tl.etl.Formula;
import org.eclipse.trace4cps.ui.view.TraceView;

public final class SpecNode {
    private final TraceView view;

    private final File specFile;

    private final List<CheckNode> checks = new ArrayList<>();

    private VerificationSemantics sem;

    public SpecNode(File file, TraceView view, VerificationSemantics sem) {
        this.specFile = file;
        this.view = view;
        this.sem = sem;
    }

    public File getSpecFile() {
        return specFile;
    }

    public List<CheckNode> getChecks() {
        return checks;
    }

    public void add(VerificationResult res) {
        long timeStamp = System.currentTimeMillis();
        boolean updated = false;
        for (MtlFormula check: res.getChecks()) {
            Formula etl = res.getEtlFromCheck(check);
            String name = res.getName(check);
            CheckNode n = getOrCreateNode(name);
            n.set(timeStamp, check, res, etl);
            updated = true;
        }
        if (updated) {
            Collections.sort(checks, Comparator.comparing(CheckNode::getCheckName));
        }
    }

    private CheckNode getOrCreateNode(String name) {
        CheckNode n = null;
        for (CheckNode cn: checks) {
            if (cn.getCheckName().equals(name)) {
                n = cn;
                break;
            }
        }
        if (n == null) {
            n = new CheckNode(view);
            checks.add(n);
        }
        return n;
    }

    public boolean refreshSpecNode(long lastModified) {
        lastModified = Math.max(lastModified, specFile.lastModified());

        List<CheckNode> toRemoveCn = new ArrayList<>();
        for (CheckNode cn: checks) {
            if (cn.getCheckTime() <= lastModified) {
                toRemoveCn.add(cn);
            }
        }
        checks.removeAll(toRemoveCn);

        if (checks.isEmpty()) {
            this.sem = null;
        }

        return !specFile.exists();
    }

    public String getLabel() {
        if (sem == null) {
            return specFile.getName();
        } else {
            String s;
            switch (sem) {
                case Finite:
                    s = "finite";
                    break;
                case ThreeValued:
                    s = "3-valued";
                    break;
                case FourValued:
                    s = "4-valued";
                    break;
                default:
                    throw new IllegalStateException();
            }
            return String.format("%s (%s)", specFile.getName(), s);
        }
    }

    public TraceView getView() {
        return view;
    }

    public VerificationSemantics getSemantics() {
        return sem;
    }
}
