/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.view.action;

import org.eclipse.trace4cps.core.TraceException;
import org.eclipse.trace4cps.ui.view.TraceView;
import org.eclipse.trace4cps.ui.view.verify.VerifyUtil;

public class VerifyAction extends AbstractTraceViewAction {
    public VerifyAction(TraceView view) {
        super(view, "/icons/passed.png");
        setToolTipText("Verify");
    }

    @Override
    public boolean isEnabled() {
        return view.getNumTraces() == 1;
    }

    @Override
    protected void doRun() throws TraceException {
        VerifyUtil.verifyChooseFile(view);
    }
}
