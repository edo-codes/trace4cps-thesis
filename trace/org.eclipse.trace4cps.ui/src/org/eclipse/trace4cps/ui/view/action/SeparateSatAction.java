/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.view.action;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.trace4cps.core.TraceException;
import org.eclipse.trace4cps.core.TracePart;
import org.eclipse.trace4cps.ui.view.TraceView;

public class SeparateSatAction extends AbstractTraceViewAction {
    public SeparateSatAction(TraceView view) {
        super(view, "/icons/separate_sat.png");
        setToolTipText("Separate explanation lanes by satisfaction");
    }

    @Override
    public boolean isEnabled() {
        return view.hasExtension(TracePart.EVENT);
    }

    @Override
    protected void doRun() throws TraceException {
        Set<String> groupAtts = new HashSet<>();
        Collection<String> prevGroupAtts = view.getViewConfiguration().getGroupingAttributes(TracePart.EVENT);
        if (prevGroupAtts != null) {
            groupAtts.addAll(prevGroupAtts);
        }
        if (groupAtts.contains("sat")) {
            groupAtts.remove("sat");
        } else {
            groupAtts.add("sat");
        }
        view.getViewConfiguration().setGroupingAttributes(TracePart.EVENT, groupAtts);
        view.update();
    }
}
