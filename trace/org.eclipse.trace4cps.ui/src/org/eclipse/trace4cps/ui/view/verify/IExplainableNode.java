/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.view.verify;

import org.eclipse.trace4cps.analysis.mtl.MtlFormula;
import org.eclipse.trace4cps.tl.VerificationResult;
import org.eclipse.trace4cps.ui.view.TraceView;

public interface IExplainableNode {
    public String getExplanationLabel();

    public MtlFormula getFormula();

    public MtlFormula getCheck();

    public VerificationResult getResult();

    public TraceView getTraceView();

    public String getCheckName();
}
