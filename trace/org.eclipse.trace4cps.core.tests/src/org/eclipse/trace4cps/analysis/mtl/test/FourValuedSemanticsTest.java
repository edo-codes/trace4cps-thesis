/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.mtl.test;

import static org.eclipse.trace4cps.analysis.mtl.MtlBuilder.F;
import static org.eclipse.trace4cps.analysis.mtl.MtlBuilder.FALSE;
import static org.eclipse.trace4cps.analysis.mtl.MtlBuilder.G;
import static org.eclipse.trace4cps.analysis.mtl.MtlBuilder.NOT;
import static org.eclipse.trace4cps.analysis.mtl.MtlBuilder.TRUE;
import static org.eclipse.trace4cps.analysis.mtl.MtlBuilder.U;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.trace4cps.analysis.mtl.DefaultAtomicProposition;
import org.eclipse.trace4cps.analysis.mtl.InformativePrefix;
import org.eclipse.trace4cps.analysis.mtl.MtlChecker;
import org.eclipse.trace4cps.analysis.mtl.MtlFormula;
import org.eclipse.trace4cps.analysis.mtl.State;
import org.eclipse.trace4cps.analysis.mtl.VerificationSemantics;
import org.eclipse.trace4cps.analysis.mtl.check.test.DefaultState;
import org.eclipse.trace4cps.core.impl.Interval;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class FourValuedSemanticsTest {
    private MtlChecker c;

    @BeforeEach
    public void setup() {
        c = new MtlChecker(1);
    }

    @Test
    public void fourEqualsThreePlusFinite() {
        List<State> trace = new ArrayList<State>();
        trace.add(new DefaultState(0));

        List<MtlFormula> formulas = new ArrayList<MtlFormula>();
        formulas.add(U(TRUE(), TRUE()));
        formulas.add(U(TRUE(), FALSE()));
        formulas.add(U(FALSE(), TRUE()));
        formulas.add(U(FALSE(), FALSE()));

        formulas.add(NOT(U(TRUE(), FALSE())));

        formulas.add(U(TRUE(), U(TRUE(), FALSE())));
        formulas.add(U(FALSE(), U(TRUE(), FALSE())));
        formulas.add(U(TRUE(), NOT(U(TRUE(), FALSE()))));
        formulas.add(U(FALSE(), NOT(U(TRUE(), FALSE()))));
        formulas.add(U(U(TRUE(), FALSE()), TRUE()));
        formulas.add(U(U(TRUE(), FALSE()), FALSE()));
        formulas.add(U(NOT(U(TRUE(), FALSE())), TRUE()));
        formulas.add(U(NOT(U(TRUE(), FALSE())), FALSE()));

        formulas.add(F(FALSE()));
        formulas.add(F(TRUE()));
        formulas.add(G(FALSE()));
        formulas.add(G(TRUE()));
        formulas.add(G(G(FALSE())));
        formulas.add(G(G(TRUE())));
        formulas.add(F(F(FALSE())));
        formulas.add(F(F(TRUE())));
        formulas.add(G(F(FALSE())));
        formulas.add(G(F(TRUE())));
        formulas.add(F(G(FALSE())));
        formulas.add(F(G(TRUE())));
        formulas.add(G(G(G(G(TRUE())))));

        checkFormulasFour(trace, formulas);

        List<State> trace2 = new ArrayList<State>();
        trace2.add(new DefaultState(0, "e", "1"));
        trace2.add(new DefaultState(1, "e", "2"));

        DefaultAtomicProposition e1 = new DefaultAtomicProposition("e", "1");
        DefaultAtomicProposition e2 = new DefaultAtomicProposition("e", "2");

        Interval i = new Interval(0.0, 0.5);

        List<MtlFormula> formulas2 = new ArrayList<MtlFormula>();
        formulas2.add(U(e1, U(e1, e2, i), i));
        formulas2.add(U(e2, U(e1, e2, i), i));
        formulas2.add(U(e1, NOT(U(e1, e2, i)), i));
        formulas2.add(U(e2, NOT(U(e1, e2, i)), i));
        formulas2.add(U(U(e1, e2, i), e1, i));
        formulas2.add(U(U(e1, e2, i), e2, i));
        formulas2.add(U(NOT(U(e1, e2, i)), e1, i));
        formulas2.add(U(NOT(U(e1, e2, i)), e2, i));
        formulas2.add(U(e2, U(e2, e1, i), i));
        formulas2.add(U(e1, U(e2, e1, i), i));
        formulas2.add(U(e2, NOT(U(e2, e1, i)), i));
        formulas2.add(U(e1, NOT(U(e2, e1, i)), i));
        formulas2.add(U(U(e2, e1, i), e2, i));
        formulas2.add(U(U(e2, e1, i), e1, i));
        formulas2.add(U(NOT(U(e2, e1, i)), e2, i));
        formulas2.add(U(NOT(U(e2, e1, i)), e1, i));

        checkFormulasFour(trace2, formulas2);
    }

    @Test
    public void nestedUntil() {
        List<State> trace = new ArrayList<State>();
        trace.add(new DefaultState(0));

        List<MtlFormula> formulas = new ArrayList<MtlFormula>();
        MtlFormula stillfalse = U(TRUE(), FALSE());
        MtlFormula stilltrue = NOT(stillfalse);
        formulas.add(U(TRUE(), stillfalse));
        formulas.add(U(TRUE(), stilltrue));

        checkFormulasFour(trace, formulas);
    }

    private void checkFormulasFour(List<State> trace, Collection<MtlFormula> formulas) {
        for (MtlFormula phi: formulas) {
            System.out.format("%-50s    ", phi);
            InformativePrefix finite = c.check(trace, phi, null, VerificationSemantics.Finite).informative();
            System.out.format("f: %-5s  ", finite);
            InformativePrefix three = c.check(trace, phi, null, VerificationSemantics.ThreeValued).informative();
            System.out.format("3: %-15s  ", three);
            InformativePrefix four = c.check(trace, phi, null, VerificationSemantics.FourValued).informative();
            System.out.format("4: %-15s\n", four);
            if (three == InformativePrefix.NON_INFORMATIVE) {
                if (finite == InformativePrefix.TRUE) {
                    assertEquals(four, InformativePrefix.STILL_TRUE);
                } else {
                    assertEquals(four, InformativePrefix.STILL_FALSE);
                }
            } else {
                assertEquals(three, finite);
                assertEquals(four, finite);
            }
        }
    }
}
