/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.core.impl;

import org.eclipse.trace4cps.analysis.mtl.AtomicProposition;
import org.eclipse.trace4cps.core.IEvent;

/**
 * Default implementation of the {@link IEvent} type.
 */
public class Event extends AttributeAware implements IEvent {
    private Number timestamp;

    public Event(Number timestamp) {
        this.timestamp = timestamp;
    }

    public Event(Number timestamp, String... properties) {
        this.timestamp = timestamp;
        if (properties != null) {
            for (int i = 0; i < properties.length; i += 2) {
                this.setAttribute(properties[i], properties[i + 1]);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Number getTimestamp() {
        return timestamp;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean satisfies(AtomicProposition p) {
        if (p.getType() == null) {
            return TraceHelper.matches(p, this);
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "Event[t=" + timestamp + ", attributes=" + getAttributes() + "]";
    }
}
