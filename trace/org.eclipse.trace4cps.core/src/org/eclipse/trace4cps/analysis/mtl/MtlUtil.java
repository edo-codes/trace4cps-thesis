/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.mtl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.trace4cps.analysis.mtl.ExplanationTable.Region;
import org.eclipse.trace4cps.analysis.mtl.impl.MTLand;
import org.eclipse.trace4cps.analysis.mtl.impl.MTLimply;
import org.eclipse.trace4cps.analysis.mtl.impl.MTLnext;
import org.eclipse.trace4cps.analysis.mtl.impl.MTLnot;
import org.eclipse.trace4cps.analysis.mtl.impl.MTLor;
import org.eclipse.trace4cps.analysis.mtl.impl.MTLtrue;
import org.eclipse.trace4cps.analysis.mtl.impl.MTLuntil;
import org.eclipse.trace4cps.analysis.stl.StlFormula;
import org.eclipse.trace4cps.analysis.stl.impl.StlUntil;

/**
 * A utility class for handling MTL-related things.
 */
public final class MtlUtil {
    private MtlUtil() {
    }

    public static boolean isMtl(MtlFormula phi) {
        for (MtlFormula sub: getSubformulas(phi)) {
            if (sub instanceof StlFormula) {
                return false;
            }
        }
        return true;
    }

    public static boolean isStl(MtlFormula phi) {
        return phi instanceof StlFormula;
    }

    public static boolean isStlMx(MtlFormula phi) {
        return !(isStl(phi) || isMtl(phi));
    }

    public static boolean isTimed(MtlFormula phi) {
        for (MtlFormula sub: getSubformulas(phi)) {
            if (sub instanceof MTLuntil && !((MTLuntil)sub).getInterval().isTrivial()) {
                return true;
            } else if (sub instanceof StlUntil && !((StlUntil)sub).isUntimed()) {
                return true;
            }
        }
        return false;
    }

    public static Collection<Integer> getAtomicPropIndices(MtlResult r) {
        Set<AtomicProposition> aps = getAtomicPropositions(r.getPhi());
        ExplanationTable tab = r.getExplanation();
        Set<Integer> result = new HashSet<Integer>();
        for (AtomicProposition ap: aps) {
            for (Region region: tab.getRegions(ap)) {
                if (region.getValue() == InformativePrefix.TRUE) {
                    for (int i = region.getStartIndex(); i <= region.getEndIndex(); i++) {
                        result.add(i);
                    }
                }
            }
        }
        return result;
    }

    /**
     * Returns all subformulas of a given formula in a depth-first order (including the formula itself).
     *
     * @param phi the formula
     * @return a list of subformulas
     */
    public static List<MtlFormula> getSubformulas(MtlFormula phi) {
        List<MtlFormula> r = new ArrayList<MtlFormula>();
        depthFirstSub(phi, r);
        return r;
    }

    public static Set<AtomicProposition> getAtomicPropositions(MtlFormula phi) {
        Set<AtomicProposition> r = new HashSet<AtomicProposition>();
        depthFirstAP(phi, r);
        return r;
    }

    private static void depthFirstAP(MtlFormula phi, Set<AtomicProposition> r) {
        if (!phi.getChildren().isEmpty()) {
            for (MtlFormula c: phi.getChildren()) {
                depthFirstAP(c, r);
            }
        } else if (phi instanceof AtomicProposition) {
            r.add((AtomicProposition)phi);
        }
    }

    private static void depthFirstSub(MtlFormula phi, List<MtlFormula> r) {
        if (!phi.getChildren().isEmpty()) {
            for (MtlFormula c: phi.getChildren()) {
                depthFirstSub(c, r);
            }
        }
        if (!r.contains(phi)) {
            r.add(phi);
        }
    }

    public static MtlFormula fixDoubleNot(MtlFormula phi) {
        while (phi instanceof MTLnot && ((MTLnot)phi).getChild() instanceof MTLnot) {
            phi = ((MTLnot)((MTLnot)phi).getChild()).getChild();
        }
        if (phi instanceof MTLand) {
            MTLand p = (MTLand)phi;
            return new MTLand(fixDoubleNot(p.getLeft()), fixDoubleNot(p.getRight()));
        } else if (phi instanceof MTLimply) {
            MTLimply p = (MTLimply)phi;
            return new MTLimply(fixDoubleNot(p.getLeft()), fixDoubleNot(p.getRight()));
        } else if (phi instanceof MTLor) {
            MTLor p = (MTLor)phi;
            return new MTLor(fixDoubleNot(p.getLeft()), fixDoubleNot(p.getRight()));
        } else if (phi instanceof MTLnext) {
            MTLnext p = (MTLnext)phi;
            return new MTLnext(fixDoubleNot(p.getChild()));
        } else if (phi instanceof MTLuntil) {
            MTLuntil p = (MTLuntil)phi;
            return new MTLuntil(fixDoubleNot(p.getLeft()), fixDoubleNot(p.getRight()), p.getInterval());
        } else if (phi instanceof MTLnot) {
            MTLnot p = (MTLnot)phi;
            return new MTLnot(fixDoubleNot(p.getChild()));
        } else if (phi instanceof MTLtrue) {
            return phi;
        } else if (phi instanceof DefaultAtomicProposition) {
            return phi;
        } else {
            throw new IllegalArgumentException("Unknown MtlFormula type");
        }
    }

    public static String getLabel(MtlFormula phi) {
        if (phi instanceof MTLand) {
            return "and";
        } else if (phi instanceof MTLimply) {
            return "if ... then";
        } else if (phi instanceof MTLor) {
            return "or";
        } else if (phi instanceof MTLnext) {
            return "next";
        } else if (phi instanceof MTLuntil) {
            return "until_" + ((MTLuntil)phi).getInterval().toString();
        } else if (phi instanceof MTLnot) {
            return "not";
        } else if (phi instanceof MTLtrue) {
            return "true";
        } else if (phi instanceof DefaultAtomicProposition) {
            return phi.toString();
        } else {
            throw new IllegalArgumentException("Unknown MtlFormula type");
        }
    }
}
