/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.mtl.causality

import com.google.common.base.Strings
import com.google.common.collect.HashBasedTable
import com.google.common.collect.Table
import java.util.HashSet
import java.util.List
import java.util.Set
import org.eclipse.trace4cps.analysis.mtl.State
import java.util.ArrayList

class LtlCausalityChecker {
    List<? extends State> trace
    final Set<Cause> causes = new HashSet
    Table<Integer, LtlNnf, Boolean> valTable
    int depth = 0
    boolean newU
    boolean finite
    boolean verbose = false
    ArrayList<String> log = new ArrayList

    private new() {
    }

    /*
     * Computes an approximation of the set of causes (event/AP-pairs) for the falsification of {@code phi}.
     * {@code phi} must be an LTL formula in Negative Normal Form, which can be obtained by using {@link #toNormalForm}
     */
    def static Set<Cause> computeCauses(List<? extends State> trace, LtlNnf phi) {
        computeCauses(trace, phi, false, false)
    }

    def static Set<Cause> computeCauses(List<? extends State> trace, LtlNnf phi, boolean newU, boolean finite) {
        val checker = new LtlCausalityChecker
        checker.trace = trace
        checker.valTable = HashBasedTable.create(trace.size, (phi.subformulas.count as double * 2) as int)
        checker.newU = newU
        checker.finite = finite
        checker.C(0, phi, true)
        if (checker.verbose) {
            checker.log.forEach[l|System.out.println(l)]
        }
        return checker.causes
    }

    def static boolean checkLTL(List<? extends State> trace, LtlNnf phi, boolean newU, boolean finite) {
        val checker = new LtlCausalityChecker
        checker.trace = trace
        checker.valTable = HashBasedTable.create(trace.size, (phi.subformulas.count as double * 2) as int)
        checker.newU = newU
        checker.finite = finite
        val v = checker.C(0, phi, false)
        if (checker.verbose) {
            checker.log.forEach[l|System.out.println(l)]
        }
        return v
    }

    /*
     * Computes an approximation of the set of causes for the falsification of phi
     * on the suffix of the trace starting at index.
     * Returns true if the set of causes found in this (sub)formula is non-empty.
     * If recordCauses == true, the set of causes is stored in this.causes
     */
    def private boolean C(int index, LtlNnf phi, boolean recordCauses) {
        var int logindex
        if (verbose) {
            log.add(String.format(
                "%s %s %d:  (%s)",
                Strings.repeat(" ", depth),
                if (recordCauses) "C" else "val",
                index,
                phi
            ))
            logindex = log.size - 1
        }
        depth++

        var v = valTable.get(index, phi)
        if (v === null || recordCauses) {
            v = switch phi {
                NnfTrue:
                    true
                NnfNot case phi.child instanceof NnfTrue:
                    false
                NnfAP | NnfAPMany: {
                    val ap = phi.toMtlAP
                    if (!trace.get(index).satisfies(ap)) {
                        val cause = new Cause(index, ap)
                        if (recordCauses) {
                            causes.add(cause)
                            if (verbose) {
                                log.add(Strings.repeat(" ", depth) + "Add cause " + cause)
                            }
                        }
                        false
                    } else {
                        true
                    }
                }
                NnfNot case phi.child instanceof NnfFormulaAP: {
                    val ap = (phi.child as NnfFormulaAP).toMtlAP
                    if (trace.get(index).satisfies(ap)) {
                        val cause = new Cause(index, ap)
                        if (recordCauses) {
                            causes.add(cause)
                            if (verbose) {
                                log.add(Strings.repeat(" ", depth) + "Add cause " + cause)
                            }
                        }
                        false
                    } else {
                        true
                    }
                }
                NnfNext case !finite:
                    if (index < trace.size - 1)
                        C(index + 1, phi.child, recordCauses)
                    else
                        true
                NnfNext case finite:
                    if (index < trace.size - 1)
                        C(index + 1, phi.child, recordCauses)
                    else
                        false
                NnfWeakNext:
                    if (index < trace.size - 1)
                        C(index + 1, phi.child, recordCauses)
                    else
                        true
                NnfAnd: {
                    val v1 = C(index, phi.left, recordCauses)
                    val v2 = C(index, phi.right, recordCauses)
                    v1 && v2
                }
                NnfOr case !newU:
                    if (!C(index, phi.left, false) && !C(index, phi.right, false)) {
                        val v1 = C(index, phi.left, recordCauses)
                        val v2 = C(index, phi.right, recordCauses)
                        v1 && v2
                    } else {
                        true
                    }
                NnfOr case newU:
                    if (!C(index, phi.left, false) && !C(index, phi.right, false)) {
                        if (recordCauses) {
                            C(index, phi.left, true)
                            C(index, phi.right, true)
                        }
                        false
                    } else {
                        true
                    }
                NnfGlobally case !newU: {
                    val v1 = C(index, phi.child, false)
                    if (!v1)
                        C(index, phi.child, recordCauses)
                    else if (v1 && index < trace.size - 1 && !C(index, new NnfNext(phi), false)) {
                        C(index + 1, phi, recordCauses)
                    } else
                        true
                }
                NnfGlobally case newU: {
                    val v1 = C(index, phi.child, false)
                    if (!v1) {
                        if (recordCauses)
                            C(index, phi.child, true)
                        false
                    } else if (v1 && index < trace.size - 1 && !C(index + 1, phi, false)) {
                        if (recordCauses)
                            C(index + 1, phi, true)
                        false
                    } else
                        true
                }
                NnfUntil case !newU: {
                    val v1 = C(index, phi.left, false)
                    val v2 = C(index, phi.right, false)
                    if (!v1 && !v2) {
                        union(
                            C(index, phi.right, recordCauses),
                            C(index, phi.left, recordCauses)
                        )
                    } else if (v1 && !v2 && index == trace.size - 1) {
                        C(index, phi.right, recordCauses)
                    } else if (v1 && !v2 && index < trace.size - 1 && !C(index, new NnfNext(phi), false)) {
                        union(
                            C(index, phi.right, recordCauses),
                            C(index + 1, phi, recordCauses)
                        )
                    } else {
                        true
                    }
                }
                NnfUntil case newU: {
                    val v1 = C(index, phi.left, false)
                    val v2 = C(index, phi.right, false)
                    if (!v1 && !v2) {
                        if (recordCauses) {
                            C(index, phi.right, true)
                            C(index, phi.left, true)
                        }
                        false
                    } else if (v1 && !v2 && index == trace.size - 1) {
                        if (recordCauses)
                            C(index, phi.right, true)
                        false
                    } else if (v1 && !v2 && index < trace.size - 1 && !C(index + 1, phi, false)) {
                        if (recordCauses) {
                            C(index, phi.right, true)
                            C(index + 1, phi, true)
                        }
                        false
                    } else {
                        true
                    }
                }
                NnfRelease case !newU: {
                    val v1 = C(index, phi.left, false)
                    val v2 = C(index, phi.right, false)
                    if (!v1 && !v2) {
                        union(
                            C(index, phi.right, recordCauses),
                            C(index, phi.left, recordCauses)
                        )
                    } else if (v1 && !v2) {
                        C(index, phi.right, recordCauses)
                    } else if (!v1 && v2 && index < trace.size - 1 && !C(index, new NnfWeakNext(phi), false)) {
                        union(
                            C(index, phi.left, recordCauses),
                            C(index + 1, phi, recordCauses)
                        )
                    } else {
                        true
                    }
                }
                NnfRelease case newU: {
                    val v1 = C(index, phi.left, false)
                    val v2 = C(index, phi.right, false)
                    if (!v1 && !v2) {
                        if (recordCauses) {
                            C(index, phi.right, true)
                            C(index, phi.left, true)
                        }
                        false
                    } else if (v1 && !v2) {
                        if (recordCauses)
                            C(index, phi.right, true)
                        false
                    } else if (!v1 && v2 && index < trace.size - 1 && !C(index + 1, phi, false)) {
                        if (recordCauses) {
                            C(index, phi.left, true)
                            C(index + 1, phi, true)
                        }
                        false
                    } else {
                        true
                    }
                }
                default:
                    throw new IllegalStateException()
            }
            valTable.put(index, phi, v)
        } else {
            if (verbose) {
                log.set(logindex, log.get(logindex) + " (mem)")
            }
        }

        depth--
        if (verbose) {
            log.set(logindex, log.get(logindex) + " " + v)
        }
        v
    }

    def static private boolean union(
        boolean v1,
        boolean v2
    ) {
        return v1 && v2;
    }
}
