/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.mtl.check;

import static org.eclipse.trace4cps.analysis.mtl.check.TabularExplanationTable.MAYBE;
import static org.eclipse.trace4cps.analysis.mtl.check.TabularExplanationTable.NO;
import static org.eclipse.trace4cps.analysis.mtl.check.TabularExplanationTable.STILLNO;
import static org.eclipse.trace4cps.analysis.mtl.check.TabularExplanationTable.STILLYES;
import static org.eclipse.trace4cps.analysis.mtl.check.TabularExplanationTable.UNKNOWN;
import static org.eclipse.trace4cps.analysis.mtl.check.TabularExplanationTable.YES;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.trace4cps.analysis.mtl.AtomicProposition;
import org.eclipse.trace4cps.analysis.mtl.ExplanationTable;
import org.eclipse.trace4cps.analysis.mtl.InformativePrefix;
import org.eclipse.trace4cps.analysis.mtl.MtlFormula;
import org.eclipse.trace4cps.analysis.mtl.MtlUtil;
import org.eclipse.trace4cps.analysis.mtl.State;
import org.eclipse.trace4cps.analysis.mtl.VerificationSemantics;
import org.eclipse.trace4cps.analysis.mtl.impl.MTLand;
import org.eclipse.trace4cps.analysis.mtl.impl.MTLimply;
import org.eclipse.trace4cps.analysis.mtl.impl.MTLnext;
import org.eclipse.trace4cps.analysis.mtl.impl.MTLnot;
import org.eclipse.trace4cps.analysis.mtl.impl.MTLor;
import org.eclipse.trace4cps.analysis.mtl.impl.MTLtrue;
import org.eclipse.trace4cps.analysis.mtl.impl.MTLuntil;
import org.eclipse.trace4cps.analysis.signal.impl.PsopHelper;
import org.eclipse.trace4cps.analysis.stl.StlFormula;
import org.eclipse.trace4cps.core.impl.Interval;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.ImmutableSet;

public final class RecursiveMemoizationChecker implements SingleFormulaChecker {
    private List<? extends State> trace;

    private Set<InformativePrefix> log;

    private VerificationSemantics sem;

    private TabularExplanationTable l;

    private final BiMap<MtlFormula, Integer> idMap = HashBiMap.create();

    private final Map<MtlFormula, Integer> depthMap = new HashMap<MtlFormula, Integer>();

    @Override
    public InformativePrefix check(List<? extends State> trace, MtlFormula phi, Set<InformativePrefix> log,
            VerificationSemantics sem)
    {
        this.trace = trace;
        if (log != null) {
            this.log = log;
        } else {
            this.log = Collections.emptySet();
        }
        this.sem = sem;

        List<MtlFormula> sub = MtlUtil.getSubformulas(phi);
        idMap.clear();
        int i = 0;
        for (MtlFormula f: sub) {
            idMap.put(f, i);
            i++;
        }
        fillDepthMap(phi);
        this.l = new CompactExplanationTableImpl(trace, idMap);
        // Now compute the truth value of the top-level formula depth first
        // filling needed table entries on-the-fly:
        fillTable(phi, 0);
        InformativePrefix result = l.getValue(phi, 0);
        if (!this.log.contains(result)) {
            // throw away the explanation
            l = null;
        }
        return result;
    }

    @Override
    public void checkOnAllEvents(List<? extends State> trace, Collection<AtomicProposition> phis) {
        this.trace = trace;
        this.log = ImmutableSet.of(InformativePrefix.TRUE, InformativePrefix.FALSE);
        this.sem = VerificationSemantics.Finite;
        idMap.clear();
        int formulaIndex = 0;
        for (AtomicProposition atom: phis) {
            idMap.put(atom, formulaIndex);
            depthMap.put(atom, 0);
            formulaIndex++;
        }
        this.l = new CompactExplanationTableImpl(trace, idMap);
        for (int i = 0; i < trace.size(); i++) {
            for (AtomicProposition atom: phis) {
                fillAP(atom, i);
            }
        }
    }

    private int fillDepthMap(MtlFormula phi) {
        int max = 0;
        for (MtlFormula child: phi.getChildren()) {
            max = Math.max(max, fillDepthMap(child));
        }
        depthMap.put(phi, max);
        return max;
    }

    private int getDepth(MtlFormula phi) {
        Integer depth = depthMap.get(phi);
        if (depth != null) {
            return depth;
        }
        throw new IllegalStateException(phi + " not present in depth map");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExplanationTable getExplanation() {
        return l;
    }

    private int fillTable(MtlFormula f, int i) {
        if (f instanceof AtomicProposition) {
            return fillAP((AtomicProposition)f, i);
        } else if (f instanceof MTLnot) {
            return fillNot((MTLnot)f, i);
        } else if (f instanceof MTLand) {
            return fillAnd((MTLand)f, i);
        } else if (f instanceof MTLimply) {
            return fillImply((MTLimply)f, i);
        } else if (f instanceof MTLor) {
            return fillOr((MTLor)f, i);
        } else if (f instanceof MTLnext) {
            return fillNext((MTLnext)f, i);
        } else if (f instanceof MTLuntil) {
            return fillUntil((MTLuntil)f, i);
        } else if (f instanceof StlFormula) {
            return fillStlSubformula((StlFormula)f, i);
        } else if (f instanceof MTLtrue) {
            return fillTrue((MTLtrue)f, i);
        } else {
            throw new IllegalStateException("use of " + f.getClass().toString() + " constructs unsupported!");
        }
    }

    private int fillTrue(MTLtrue f, int i) {
        int formulaIndex = idMap.get(f);
        return l.put(formulaIndex, i, YES);
    }

    private int fillAP(AtomicProposition f, int i) {
        int formulaIndex = idMap.get(f);
        if (trace.get(i).satisfies(f)) {
            return l.put(formulaIndex, i, YES);
        } else {
            return l.put(formulaIndex, i, NO);
        }
    }

    private int fillStlSubformula(StlFormula f, int i) {
        int formulaIndex = idMap.get(f);
        double t = trace.get(i).getTimestamp().doubleValue();
        if (!PsopHelper.dom(f.getSignal()).contains(t)) {
            throw new IllegalStateException("incompatible time domain");
        }
        double rho = PsopHelper.valueAt(f.getSignal(), t).doubleValue();
        if (rho >= 0) {
            return l.put(formulaIndex, i, YES);
        } else {
            return l.put(formulaIndex, i, NO);
        }
    }

    private int fillNot(MTLnot f, int i) {
        int childIndex = idMap.get(f.getChild());
        int formulaIndex = idMap.get(f);
        int v = l.get(childIndex, i);
        if (v == UNKNOWN) {
            v = fillTable(f.getChild(), i);
        }
        return l.put(formulaIndex, i, not(v));
    }

    private int fillOr(MTLor f, int i) {
        MtlFormula f1 = f.getLeft();
        MtlFormula f2 = f.getRight();
        int v1 = l.get(idMap.get(f1), i);
        int v2 = l.get(idMap.get(f2), i);
        // First check to compute unknowns with least depth first:
        if (v1 == UNKNOWN && v2 == UNKNOWN) {
            if (getDepth(f1) <= getDepth(f2)) {
                v1 = fillTable(f1, i);
            } else {
                v2 = fillTable(f2, i);
            }
        }
        // Check whether we need to fill the other unknown:
        if (v1 == UNKNOWN && v2 != YES) {
            v1 = fillTable(f1, i);
        } else if (v2 == UNKNOWN && v1 != YES) {
            v2 = fillTable(f2, i);
        }
        return l.put(idMap.get(f), i, or(v1, v2));
    }

    private int fillAnd(MTLand f, int i) {
        MtlFormula f1 = f.getLeft();
        MtlFormula f2 = f.getRight();
        int v1 = l.get(idMap.get(f1), i);
        int v2 = l.get(idMap.get(f2), i);
        // First check to compute unknowns with least depth first:
        if (v1 == UNKNOWN && v2 == UNKNOWN) {
            if (getDepth(f1) <= getDepth(f2)) {
                v1 = fillTable(f1, i);
            } else {
                v2 = fillTable(f2, i);
            }
        }
        if (v1 == UNKNOWN && v2 != NO) {
            v1 = fillTable(f1, i);
        } else if (v2 == UNKNOWN && v1 != NO) {
            v2 = fillTable(f2, i);
        }
        return l.put(idMap.get(f), i, and(v1, v2));
    }

    private int fillImply(MTLimply f, int i) {
        MtlFormula f1 = f.getLeft();
        MtlFormula f2 = f.getRight();
        int v1 = l.get(idMap.get(f1), i);
        int v2 = l.get(idMap.get(f2), i);
        // First check to compute unknowns with least depth first:
        if (v1 == UNKNOWN && v2 == UNKNOWN) {
            if (getDepth(f1) <= getDepth(f2)) {
                v1 = fillTable(f1, i);
            } else {
                v2 = fillTable(f2, i);
            }
        }
        if (v1 == UNKNOWN && v2 != NO) {
            v1 = fillTable(f1, i);
        } else if (v2 == UNKNOWN && v1 != NO) {
            v2 = fillTable(f2, i);
        }
        return l.put(idMap.get(f), i, implies(v1, v2));
    }

    private int fillNext(MTLnext f, int i) {
        int formulaIndex = idMap.get(f);
        if (i == trace.size() - 1) {
            if (sem == VerificationSemantics.Finite) {
                return l.put(formulaIndex, i, NO);
            } else if (sem == VerificationSemantics.ThreeValued) {
                return l.put(formulaIndex, i, MAYBE);
            } else {
                return l.put(formulaIndex, i, STILLNO);
            }
        } else {
            int childIndex = idMap.get(f.getChild());
            int v = l.get(childIndex, i + 1);
            if (v == UNKNOWN) {
                v = fillTable(f.getChild(), i + 1);
            }
            return l.put(formulaIndex, i, v);
        }
    }

    private int fillUntil(MTLuntil f, int i) {
        if (sem == VerificationSemantics.Finite) {
            return fillUntilEntryFinite(f, i);
        } else if (sem == VerificationSemantics.ThreeValued) {
            return fillUntilEntryThree(f, i);
        } else {
            return fillUntilEntryFour(f, i);
        }
    }

    private int fillUntilEntryThree(MTLuntil f, int i) {
        double ti = trace.get(i).getTimestamp().doubleValue();
        double tn = trace.get(trace.size() - 1).getTimestamp().doubleValue();
        Interval interval = f.getInterval();

        int formulaIndex = idMap.get(f);
        int leftIndex = idMap.get(f.getLeft());
        int rightIndex = idMap.get(f.getRight());

        boolean c1 = false;
        boolean c11 = true;
        boolean c2 = true;
        boolean c22 = false;
        boolean c3 = interval.strictlySmallerThan(tn - ti);
        for (int j = i; j < trace.size(); j++) {
            double tj = trace.get(j).getTimestamp().doubleValue();
            int r2 = l.get(rightIndex, j);
            if (r2 == UNKNOWN) {
                r2 = fillTable(f.getRight(), j);
            }
            c1 = c1 || (r2 == YES && interval.contains(tj - ti) && c11);
            if (c1) {
                l.addInterval(formulaIndex, i, interval.lb().doubleValue() + ti, j, interval.ub().doubleValue());
                return l.put(formulaIndex, i, YES);
            }
            int r1 = l.get(leftIndex, j);
            if (r1 == UNKNOWN) {
                r1 = fillTable(f.getLeft(), j);
            }
            c2 = c2 && (r2 == NO || !interval.contains(tj - ti) || c22);
            c3 = c3 || r1 == NO;
            c11 = c11 && (r1 == YES);
            c22 = c22 || (r1 == NO);
            boolean intervalPassed = interval.strictlySmallerThan(tj - ti);
            if ((c22 || j == trace.size() - 1 || intervalPassed) && c2 && c3) {
                l.addInterval(formulaIndex, i, interval.lb().doubleValue() + ti, (intervalPassed && j > i ? j - 1 : j),
                        intervalPassed ? interval.ub().doubleValue() + ti : tj);
                return l.put(formulaIndex, i, NO);
            }
            if ((!c11 || intervalPassed) && !c1 && !c2) {
                l.addInterval(formulaIndex, i, interval.lb().doubleValue() + ti, (intervalPassed && j > i ? j - 1 : j),
                        intervalPassed ? interval.ub().doubleValue() + ti : tj);
                return l.put(formulaIndex, i, MAYBE);
            }
        }

        l.addInterval(formulaIndex, i, interval.lb().doubleValue() + ti, trace.size() - 1, interval.ub().doubleValue());
        return l.put(formulaIndex, i, MAYBE);
    }

    private int fillUntilEntryFour(MTLuntil f, int i) {
        double ti = trace.get(i).getTimestamp().doubleValue();
        double tn = trace.get(trace.size() - 1).getTimestamp().doubleValue();
        Interval interval = f.getInterval();

        int formulaIndex = idMap.get(f);
        int leftIndex = idMap.get(f.getLeft());
        int rightIndex = idMap.get(f.getRight());

        int c_lr = NO;
        int c_l = YES;
        for (int j = i; j < trace.size(); j++) {
            double tj = trace.get(j).getTimestamp().doubleValue();
            int r2 = l.get(rightIndex, j);
            if (r2 == UNKNOWN) {
                r2 = fillTable(f.getRight(), j);
            }
            c_lr = or(c_lr, and(and(r2, toB4(interval.contains(tj - ti))), c_l));
            if (c_lr == YES) {
                l.addInterval(formulaIndex, i, interval.lb().doubleValue() + ti, j, interval.ub().doubleValue());
                return l.put(formulaIndex, i, YES);
            }
            int r1 = l.get(leftIndex, j);
            if (r1 == UNKNOWN) {
                r1 = fillTable(f.getLeft(), j);
            }
            c_l = and(c_l, r1);
            boolean intervalPassed = interval.strictlySmallerThan(tj - ti); // tj-ti ≥ sup(I)
            if ((j == trace.size() - 1 || c_l == NO || intervalPassed) && c_lr == NO
                    && (interval.strictlySmallerThan(tn - ti) || c_l == NO))
            {
                l.addInterval(formulaIndex, i, interval.lb().doubleValue() + ti, (intervalPassed && j > i ? j - 1 : j),
                        intervalPassed ? interval.ub().doubleValue() + ti : tj);
                return l.put(formulaIndex, i, NO);
            }
            if ((c_l < YES || intervalPassed) && c_lr == STILLYES) {
                l.addInterval(formulaIndex, i, interval.lb().doubleValue() + ti, (intervalPassed && j > i ? j - 1 : j),
                        intervalPassed ? interval.ub().doubleValue() + ti : tj);
                return l.put(formulaIndex, i, STILLYES);
            }
        }
        l.addInterval(formulaIndex, i, interval.lb().doubleValue() + ti, trace.size() - 1, interval.ub().doubleValue());
        return l.put(formulaIndex, i, or(c_lr, STILLNO));
    }

    @SuppressWarnings("unused")
    private int fillUntilEntryFourUnoptimized(MTLuntil f, int i) {
        double ti = trace.get(i).getTimestamp().doubleValue();
        double tn = trace.get(trace.size() - 1).getTimestamp().doubleValue();
        Interval interval = f.getInterval();

        int formulaIndex = idMap.get(f);
        int leftIndex = idMap.get(f.getLeft());
        int rightIndex = idMap.get(f.getRight());

        int c_lr = NO;
        int c_l = YES;
        for (int j = i; j < trace.size(); j++) {
            double tj = trace.get(j).getTimestamp().doubleValue();
            int r1 = l.get(leftIndex, j);
            if (r1 == UNKNOWN) {
                r1 = fillTable(f.getLeft(), j);
            }
            int r2 = l.get(rightIndex, j);
            if (r2 == UNKNOWN) {
                r2 = fillTable(f.getRight(), j);
            }
            c_lr = or(c_lr, and(and(r2, toB4(interval.contains(tj - ti))), c_l));
            c_l = and(c_l, r1);
        }
        l.addInterval(formulaIndex, i, interval.lb().doubleValue() + ti, trace.size() - 1,
                interval.ub().doubleValue() + ti);

        // return NO if violated in weak semantics, STILLNO or higher otherwise
//        if (c_lr == NO && (interval.strictlySmallerThan(tn - ti) || c_l == NO)) {
//            return l.put(formulaIndex, i, NO);
//        } else {
//            return l.put(formulaIndex, i, or(c_lr, STILLNO));
//        }
        // or equivalently:
        int v = or(c_lr, and(and(toB4(!interval.strictlySmallerThan(tn - ti)), c_l), STILLNO));
        return l.put(formulaIndex, i, v);
    }

    @SuppressWarnings("unused")
    private int fillUntilEntryThreeNewUnoptimized(MTLuntil f, int i) {
        double ti = trace.get(i).getTimestamp().doubleValue();
        double tn = trace.get(trace.size() - 1).getTimestamp().doubleValue();
        Interval interval = f.getInterval();

        int formulaIndex = idMap.get(f);
        int leftIndex = idMap.get(f.getLeft());
        int rightIndex = idMap.get(f.getRight());

        int c_lr = NO;
        int c_l = YES;
        for (int j = i; j < trace.size(); j++) {
            double tj = trace.get(j).getTimestamp().doubleValue();
            int r1 = l.get(leftIndex, j);
            if (r1 == UNKNOWN) {
                r1 = fillTable(f.getLeft(), j);
            }
            int r2 = l.get(rightIndex, j);
            if (r2 == UNKNOWN) {
                r2 = fillTable(f.getRight(), j);
            }
            c_lr = or(c_lr, and(and(r2, toB4(interval.contains(tj - ti))), c_l));
            c_l = and(c_l, r1);
        }
        l.addInterval(formulaIndex, i, interval.lb().doubleValue() + ti, trace.size() - 1,
                interval.ub().doubleValue() + ti);

        // return NO if violated in weak semantics, STILLNO or higher otherwise
//        if (c_lr == NO && (interval.strictlySmallerThan(tn - ti) || c_l == NO)) {
//            return l.put(formulaIndex, i, NO);
//        } else {
//            return l.put(formulaIndex, i, or(c_lr, STILLNO));
//        }
        // or equivalently:
        int v = or(c_lr, and(and(toB4(!interval.strictlySmallerThan(tn - ti)), c_l), MAYBE));
        return l.put(formulaIndex, i, v);
    }

    private int fillUntilEntryFourLTL(MTLuntil f, int i) {
        int formulaIndex = idMap.get(f);
        int leftIndex = idMap.get(f.getLeft());
        int rightIndex = idMap.get(f.getRight());

        int c_lr = NO;
        int c_l = YES;
        for (int j = i; j < trace.size(); j++) {
            int r2 = l.get(rightIndex, j);
            if (r2 == UNKNOWN) {
                r2 = fillTable(f.getRight(), j);
            }
            c_lr = or(c_lr, and(r2, c_l));
            if (c_lr == YES) {
                return l.put(formulaIndex, i, YES);
            }
            int r1 = l.get(leftIndex, j);
            if (r1 == UNKNOWN) {
                r1 = fillTable(f.getLeft(), j);
            }
            c_l = and(c_l, r1);
            if (c_l == NO && c_lr == NO) {
                return l.put(formulaIndex, i, NO);
            }
            if (c_l < YES && c_lr == STILLYES) {
                return l.put(formulaIndex, i, STILLYES);
            }
        }
        if (c_l == NO && c_lr == NO) {
            return l.put(formulaIndex, i, NO);
        } else {
            return l.put(formulaIndex, i, or(c_lr, STILLNO));
        }
    }

    private int fillUntilEntryFinite(MTLuntil f, int i) {
        double ti = trace.get(i).getTimestamp().doubleValue();
        Interval interval = f.getInterval();

        int formulaIndex = idMap.get(f);
        int leftIndex = idMap.get(f.getLeft());
        int rightIndex = idMap.get(f.getRight());

        boolean c1 = false;
        boolean c11 = true;
        for (int j = i; j < trace.size(); j++) {
            double tj = trace.get(j).getTimestamp().doubleValue();
            int r2 = l.get(rightIndex, j);
            if (r2 == UNKNOWN) {
                r2 = fillTable(f.getRight(), j);
            }
            c1 = c1 || (r2 == YES && interval.contains(tj - ti) && c11);
            if (c1) {
                l.addInterval(formulaIndex, i, interval.lb().doubleValue() + ti, j, tj);
                return l.put(formulaIndex, i, YES);
            }
            int r1 = l.get(leftIndex, j);
            if (r1 == UNKNOWN) {
                r1 = fillTable(f.getLeft(), j);
            }
            c11 = c11 && (r1 == YES);
            boolean intervalPassed = interval.strictlySmallerThan(tj - ti);
            if ((!c11 || j == trace.size() - 1 || intervalPassed) && !c1) {
                l.addInterval(formulaIndex, i, interval.lb().doubleValue() + ti, (intervalPassed && j > i ? j - 1 : j),
                        intervalPassed ? interval.ub().doubleValue() + ti : tj);
                return l.put(formulaIndex, i, NO);
            }
        }
        throw new IllegalStateException();
    }

    private static int not(int v) {
        if (v == YES) {
            return NO;
        } else if (v == NO) {
            return YES;
        } else if (v == STILLNO) {
            return STILLYES;
        } else if (v == STILLYES) {
            return STILLNO;
        } else { // v == MAYBE
            return MAYBE;
        }
    }

    private static int or(int v1, int v2) {
        if (v1 == YES) {
            return YES;
        } else if (v1 == NO) {
            return v2;
        } else if (v1 == STILLYES) {
            if (v2 == YES) {
                return YES;
            } else {
                return STILLYES;
            }
        } else if (v1 == STILLNO) {
            if (v2 == YES || v2 == STILLYES || v2 == MAYBE) {
                return v2;
            } else {
                return STILLNO;
            }
        } else { // v == MAYBE
            if (v2 == YES || v2 == STILLYES) {
                return v2;
            } else {
                return MAYBE;
            }
        }
    }

    private static int and(int v1, int v2) {
        if (v1 == YES) {
            return v2;
        } else if (v1 == NO) {
            return NO;
        } else if (v1 == STILLYES) {
            if (v2 == MAYBE || v2 == STILLNO || v2 == NO) {
                return v2;
            } else {
                return STILLYES;
            }
        } else if (v1 == STILLNO) {
            if (v2 == NO) {
                return NO;
            } else {
                return STILLNO;
            }
        } else { // v == MAYBE
            if (v2 == STILLNO || v2 == NO) {
                return v2;
            } else {
                return MAYBE;
            }
        }
    }

    private static int implies(int v1, int v2) {
        return or(not(v1), v2);
    }

    private int toB4(boolean v) {
        return v ? YES : NO;
    }

    @SuppressWarnings("unused")
    private String valToString(int c) {
        if (c == NO) {
            return "NO";
        } else if (c == STILLNO) {
            return "STILLNO";
        } else if (c == MAYBE) {
            return "MAYBE";
        } else if (c == UNKNOWN) {
            return "UNKNOWN";
        } else if (c == STILLYES) {
            return "STILLYES";
        } else if (c == YES) {
            return "YES";
        } else {
            throw new IllegalArgumentException();
        }
    }
}
