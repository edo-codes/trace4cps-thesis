/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.mtl.causality

import org.eclipse.trace4cps.analysis.mtl.AtomicProposition
import org.eclipse.trace4cps.analysis.mtl.DefaultAtomicProposition
import org.eclipse.trace4cps.analysis.mtl.MtlFormula
import org.eclipse.trace4cps.analysis.mtl.impl.MTLand
import org.eclipse.trace4cps.analysis.mtl.impl.MTLimply
import org.eclipse.trace4cps.analysis.mtl.impl.MTLnext
import org.eclipse.trace4cps.analysis.mtl.impl.MTLnot
import org.eclipse.trace4cps.analysis.mtl.impl.MTLor
import org.eclipse.trace4cps.analysis.mtl.impl.MTLtrue
import org.eclipse.trace4cps.analysis.mtl.impl.MTLuntil

import static org.eclipse.trace4cps.analysis.mtl.MtlBuilder.*

class MtlNnf {
    def static MtlFormula toNnf(MtlFormula phi, boolean splitAPs) {
        // First do ¬¬p ⇒ p, to make sure that the special cases for Globally in fromMtlFormula_ work
        toNnf_(phi, splitAPs);
    }

    def private static MtlFormula toNnf_(MtlFormula phi, boolean splitAPs) {
        switch phi {
            MTLtrue:
                phi
            AtomicProposition case !splitAPs: {
                phi
            }
            AtomicProposition case splitAPs: {
                // {a, b} ≡ {a} ∧ {b}
                phi.attributes.entrySet //
                .map[a|new DefaultAtomicProposition(phi.type, a.key, a.value)] //
                .reduce([MtlFormula a, MtlFormula b|AND(a, b)])
            }
            MTLor:
                OR(toNnf_(phi.left, splitAPs), toNnf_(phi.right, splitAPs))
            MTLand:
                AND(toNnf_(phi.left, splitAPs), toNnf_(phi.right, splitAPs))
            MTLimply:
                // a → b ≡ ¬a ∨ b
                OR(toNnf_(NOT(phi.left), splitAPs), toNnf_(phi.right, splitAPs))
            MTLnext:
                X(toNnf_(phi.child, splitAPs))
            MTLuntil: {
                U(toNnf_(phi.left, splitAPs), toNnf_(phi.right, splitAPs), phi.interval)
            }
            MTLnot: {
                switch phi.child {
                    MTLtrue:
                        phi
                    AtomicProposition case splitAPs: {
                        // !{a, b} ≡ !{a} ∨ !{b}
                        val p = phi.child as AtomicProposition
                        p.attributes.entrySet //
                        .map[a|NOT(new DefaultAtomicProposition(p.type, a.key, a.value))] //
                        .reduce([MtlFormula a, MtlFormula b|OR(a, b)])
                    }
                    AtomicProposition case !splitAPs: {
                        phi
                    }
                    MTLor: {
                        // ¬(a ∨ b) ≡ ¬a ∧ ¬b
                        val p = phi.child as MTLor
                        AND(toNnf_(NOT(p.left), splitAPs), toNnf_(NOT(p.right), splitAPs))
                    }
                    MTLand: {
                        // ¬(a ∧ b) ≡ ¬a ∨ ¬b
                        val p = phi.child as MTLand
                        OR(toNnf_(NOT(p.left), splitAPs), toNnf_(NOT(p.right), splitAPs))
                    }
                    MTLnot: {
                        // ¬¬a ≡ a
                        // This case should not be necessary as long as fixDoubleNot has been called before
                        toNnf_((phi.child as MTLnot).child, splitAPs)
                    }
                    MTLimply: {
                        // ¬(a → b) ≡ a ∧ ¬b
                        val p = phi.child as MTLimply
                        AND(toNnf_(p.left, splitAPs), toNnf_(NOT(p.right), splitAPs))
                    }
                    MTLnext: {
                        // ¬Xa ≡ X¬a
                        val p = phi.child as MTLnext
                        new MTLweaknext(toNnf_(NOT(p.child), splitAPs))
                    }
                    MTLuntil: {
                        // ¬(a U b) ≡ ¬a R ¬b
                        val p = phi.child as MTLuntil
                        new MTLrelease(toNnf_(NOT(p.left), splitAPs), toNnf_(NOT(p.right), splitAPs), p.interval)
                    }
                    default:
                        throw new IllegalArgumentException()
                }
            }
            default:
                throw new IllegalArgumentException()
        }
    }

    def static boolean isNnf(MtlFormula phi) {
        switch phi {
            MTLtrue:
                true
            AtomicProposition:
                true
            MTLimply:
                false
            MTLnot:
                phi.child instanceof MTLtrue || phi.child instanceof AtomicProposition
            MTLand,
            MTLnext,
            MTLweaknext,
            MTLor,
            MTLuntil,
            MTLrelease:
                phi.children.stream.allMatch[p|isNnf(p)]
            default:
                throw new IllegalArgumentException("Unknown MtlFormula class: " + phi.class)
        }
    }
}
